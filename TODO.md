# latex version (DE)
- [ ] cover
- [ ] backcover
- [x] einleitung
- [x] Bereichseinleitungen
- [x] Bereichsillustrationen
- [ ] urls zu lang -> zeilenumbruch
- [x] glossar
- [x] nachwort
- [ ] illustrationen: bessere qualität
- [x] Untergruppen Einleitungen (manuell)
- [ ] WoRaPla leer? Hinweis!

# web
- [x] startseite
- [x] illustrationen (evtl stat g1k5 logo?)
- [x] bereichsseite design
    * siehe screenshot
    * green-bg auf Maßnahmen header
- [x] "Übersetzungen notwendig" nicht i18n
- [x] i18n Bereichsanleitung
- [x] Kommentare live
- [x] "Personen" nicht i18n
- [x] Reihenfolge Maßnahmen und Unterbereiche
- [x] startseite responsive
- [x] Maßnahmen teaser enthält formatierungen
- [x] gerechigkeit illustrations
- [x] Icon für unfertige Maßnamen (Bereichsseite)
- [x] englische texte werden nicht erkannt
- [x] influxdb stats
- [ ] Unterbereich reihenfolge im Menü
- [ ] startseite illustration
- [ ] language detection

# tikiToLatex
- [x] header
- [ ] glossar links
- [x] sub / super
- [x] images
- [x] multiple references
- [x] CO² automatisch 
- [x] \{MOUSEOVER( label="CO$_2$e")\}
- [x] NOT SUPPORTED: ~~ (colored) BEGIN: ~~#0563c1:[
- [x] manche erlawa maßnahmen fehlen
- [x] {TAG( tag=ßup")}(1){TAG}
- [x] [reference not found] -> wird nun einfach ignoriert

# tikiToMakrdown
- [x] CO² automatisch`
- [x] ~~#999