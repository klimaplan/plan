# Grassroots Climate Plan - Tools
> _We are facing one of the biggest decisions of humankind - a decision on how to face the climate crisis. This is a huge responsibility and at the same time an opportunity. Our chance to work together to initiate a comprehensive change towards a more just society.._


### This repository contains...
- Release website https://klimaplanvonunten.de
- PDF generation tools
- TikiWiki converter
    * Markdown generator
    * LaTeX generator


## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
