import * as fs from 'fs'
import * as path from 'path'
import axios from 'axios'
import { connect, getPages, Page, tikiToLatex } from './tikiwiki'

import sections from './sections'


const sectionChapters = {
    reproko: 2,
    energydemocracy: 3,
    mobility: 4,
    worapla: 5,
    erlawa: 6,
    glogeint: 7
}

const outDir = path.join(__dirname, '../../pdf/')

connect({
    host     : 'localhost',
    user     : 'g1k5-wiki',
    password : process.env.MYSQL_PW,
    database : 'g1k5-wiki'
})
const lang = process.argv[2] || 'de'


async function storeLatexMeasure(section: string, p: Page) {
    const ml = p.lang[lang]
    let tikiBody = p.lang[lang].body
    tikiBody = `${tikiBody}`

    let body = await tikiToLatex(tikiBody, p.references, async (fileId) => {
        const res = await axios(`https://wiki.gerechte1komma5.de/tiki-download_file.php?fileId=${fileId}&display`, {responseType:"arraybuffer"})

        const filename = res.headers['content-disposition'].match(/"(.*?)"/)[1]
        fs.writeFileSync(path.join(outDir, 'images', filename), res.data)
        return 'images/'+filename
    })
    
    body = `\\subsection{${ml.title}}\n${body}`
    body = body.replace(/[\uF000-\uF8FF]/g, '');
    const filename = `chapters/${sectionChapters[section]}_${section}/${lang}/${ml.slug}.tex`
    fs.writeFileSync(path.join(outDir, filename), body, 'utf-8')
    // process.exit()
}
async function storeSectionStructure(section: string, subsections: Page[], allPages: Page[]) {

    let out = ''
    for(let p of subsections) {
        let childrens = allPages.filter(pc => pc.parentIds.includes(p.mainId))
        const ml = p.lang[lang]
        console.log(p.lang.de.title)
        console.log(ml.title, childrens.length)
        if(childrens.length) {
            // it is a subsection!
            out += `\\section{${ml.title}}\n`
            for(let c of childrens) {
                if(!c.lang[lang]) continue // language not available
                if (!c.tags.includes('inhaltlich_fertig')) continue
                const mlc = c.lang[lang]
                const filename = `../chapters/${sectionChapters[section]}_${section}/${lang}/${mlc.slug}.tex`
                out += `  \\input{${filename}}\n  \\clearpage\n`
            }
            out += '\n'
        }
    }
    fs.mkdirSync(path.join(outDir, `chapters/${sectionChapters[section]}_${section}/${lang}`), {recursive: true})
    fs.writeFileSync(path.join(outDir, `chapters/${sectionChapters[section]}_${section}/${lang}/structure.tex`), out, 'utf-8')

}


void async function main() {
    console.log('get pages')
    const pages = (await getPages())
    // const pages = require('/tmp/pages.json')
    // fs.writeFileSync('/tmp/pages.json', JSON.stringify(pages), 'utf-8')  
    // return
    for(let sectionId in sections) {
        const ps = pages
            .filter(p => p.parentIds.includes(parseInt(sectionId)))       
        const subsections: Page[] = []
        for(let p of ps) {
            let childrens = pages.filter(pc => pc.parentIds.includes(p.mainId))
            if(childrens.length) {
                subsections.push(p)
                for(let c of childrens) {
                    if(!c.lang[lang]) continue // language not available
                    if (c.tags.includes('inhaltlich_fertig')) {
                        // measures.push(await tikiToMeasure(c, p))
                        await storeLatexMeasure(sections[sectionId], c)
                    }
                }
            } else if (p.tags.includes('inhaltlich_fertig')) {
                // there won't be any measures on this level anymore
                // await storeLatexMeasure(p)
                // measures.push(await tikiToMeasure(p))
            }
        }
        // await storeSectionStructure(sections[sectionId], subsections, pages)
        // process.exit()
    }
    console.log('done')
    process.exit()
}()