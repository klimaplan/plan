

import { connect, getPages, Page, tikiToMarkdown } from './tikiwiki'
import { slugify, onlyUnique } from './utils'
import sections from './sections'

import * as fs from 'fs'
import * as path from 'path'

const dataDir = path.join(__dirname, '../../web/src/data')


connect({
    host     : 'localhost',
    user     : 'g1k5-wiki',
    password : process.env.MYSQL_PW,
    database : 'g1k5-wiki'
})

const pageIds = {
    preface: 269,
    preamble: 286,
    epilogue: 353,
    glossary: 281
}


void async function main() {
    const pages = await getPages()
    const sectionTexts: {[section: string]: any} = {
        preface: {},
        preamble: {},
        epilogue: {},
        glossary: {}
    }
    for(let p of pages) {
        if(sections[p.mainId]) {
            console.log(p.mainId, p.lang.de.title)
            let texts = {}
            for(let lang in p.lang) {
                let body = p.lang[lang].body
                    .replace(/^!.*?(Einleitung|Introduction).*?\r/mg, '\r')
                    .replace(/^!.*?(Maßnahmen|Measures|Contents|Inhaltsverzeichnis|Literatur|Quellen|sources)[\s\S]*/mg, '')

                // maybe we cutted of the references?
                if(!body.match(/{showreference[\s\S]*?\}/)) body += '\n\n{showreference showtitle="no"}'
                texts[lang] = await tikiToMarkdown(
                    body,
                    p.references
                )
            }
            sectionTexts[sections[p.mainId]] = texts
        }
    }

    for(let pageKey in pageIds) {
        const p = pages.find(p => p.mainId == pageIds[pageKey])
        // console.log(pageKey, sectionTexts[pageKey], !!p)
        if(!p) continue
        console.log(Object.keys(p.lang), p.lang.de.title)
        for(let lang in p.lang) {
            let body = p.lang[lang].body
                .replace(/^!.*?(Einleitung|Introduction).*?\r/mg, '\r')

            if(p.mainId !== pageIds.epilogue) {
                body = body
                    .replace(/^!.*?(Maßnahmen|Measures|Contents|Inhaltsverzeichnis|Literatur|sources)[\s\S]*/mg, '')
            }

            sectionTexts[pageKey][lang] = await tikiToMarkdown(body, p.references)    
        }
    }

    fs.writeFileSync(path.join(dataDir, 'sections.json'), JSON.stringify(sectionTexts, null, 2), 'utf-8')
    console.log('done')
    process.exit()
}()