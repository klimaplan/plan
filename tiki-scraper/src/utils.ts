import { spawn } from 'child_process'

export function slugify (str: string): string {
    return str
      .toLowerCase()
      .replace(/ä/g, 'ae')
      .replace(/ö/g, 'oe')
      .replace(/ü/g, 'ue')
      .replace(/ß/g, 'ss')
      .replace(/[^a-z0-9]/g, '-')
      .replace(/-+/g, '-')
      .replace(/(^-|-$)/g, '')
      .replace(/[^a-z0-9\-\_]/, '')
      
  }
  

export async function spawnPromised(cmd: string, args: string[], stdin: string): Promise<string> {
    return new Promise( (resolve, reject) => {
        const ps = spawn(cmd, args)
        let stdout = []
        let stderr = []
        ps.stdout.on('data', (d) => {
            stdout.push(d)
        })
        ps.stderr.on('data', (d) => {
            stderr.push(d)
        })
        ps.on('close', (code) => {
            if(code == 0) {
                resolve(Buffer.concat(stdout).toString('utf-8'))
            } else {
                resolve(Buffer.concat(stderr).toString('utf-8'))
            }
        })

        ps.stdin.write(stdin)
        ps.stdin.end()
    })
}


export function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}
