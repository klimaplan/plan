

import { connect, getPages, Page, tikiToMarkdown, tikiToLatex } from './tikiwiki'
import { slugify, onlyUnique } from './utils'
import sections from './sections'

import * as fs from 'fs'
import * as path from 'path'

const outDir = path.join(__dirname, '../../pdf/')


connect({
    host     : 'localhost',
    user     : 'g1k5-wiki',
    password : process.env.MYSQL_PW,
    database : 'g1k5-wiki'
})

const pageIds = {
    '0_vorwort': 269,
    '1_einleitung': 286,
    '8_nachwort': 353,
    '9_glossar': 281
}
const sectionChapters = {
    reproko: 2,
    energydemocracy: 3,
    mobility: 4,
    worapla: 5,
    erlawa: 6,
    glogeint: 7
}


const lang = process.argv[2] || 'de'
async function storeSectionPreamble(section: string, p: Page) {
    let body = p.lang[lang].body
    .replace(/^!.*?(Einleitung|Introduction).*?\r/mg, '\r')
    .replace(/^!.*?(Maßnahmen|Measures|Contents|Inhaltsverzeichnis|Literatur|sources)[\s\S]*/mg, '')

    const filename = `chapters/${sectionChapters[section]}_${section}/${lang}/preamble.tex`

    const latex = await tikiToLatex(body, p.references, async (s:string) => '')

    fs.writeFileSync(path.join(outDir, filename), latex, 'utf-8')
}

async function storePage(folder: string, p:Page) {
    let body = p.lang[lang].body
    .replace(/^!.*?(Einleitung|Introduction).*?\r/mg, '\r')
    // .replace(/^!.*?(Maßnahmen|Measures|Contents|Inhaltsverzeichnis|Literatur|sources)[\s\S]*/mg, '')

    if(folder == '0_vorwort') {
        body = body.replace(/^!.*?(Maßnahmen|Measures)[\s\S]*/mg, '')
    }
    if(folder == '9_glossar') {
        // subparagraph instead of paragraph
        body = body.replace(/!!!!/g, '!!!!!')
    }
    const filename = `chapters/${folder}/${lang}.tex`

    const latex = await tikiToLatex(body, p.references, async (s:string) => '')
    fs.writeFileSync(path.join(outDir, filename), latex, 'utf-8')
}

void async function main() {
    const pages = await getPages()

    for(let p of pages) {
        if(sections[p.mainId]) {
            console.log(p.mainId, p.lang.de.title)
            await storeSectionPreamble(sections[p.mainId], p)
        }
    }

    for(let folder in pageIds) {
        const p = pages.find(p => p.mainId == pageIds[folder])
        if(!p) continue
        await storePage(folder, p)
    }

    process.exit()
}()