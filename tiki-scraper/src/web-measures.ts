

import { connect, getPages, Page, tikiToMarkdown } from './tikiwiki'
import { slugify, onlyUnique } from './utils'
import sections from './sections'

import * as fs from 'fs'
import * as path from 'path'

const dataDir = path.join(__dirname, '../../web/src/data')
// if(!dataDir) {
//     console.log(`no data directory specified`)
//     process.exit(1)
// }

connect({
    host     : 'localhost',
    user     : 'g1k5-wiki',
    password : process.env.MYSQL_PW,
    database : 'g1k5-wiki'
})



interface MeasureLanguage {
    title: string
    slug: string
    group: string
    link: string
    body?: string
    teaser?: string
    lastModified: string
}
interface Measure {
    id: number
    lang: {[language: string]: MeasureLanguage} 
    translationNeeded: boolean
    substanceDone: boolean
}


async function tikiToMeasure(p: Page, parent?: Page): Promise<Measure> {
    console.log('- '+p.lang.de.title)
    const langs: {[language: string]: MeasureLanguage}  = {}
    for(let language in p.lang) {
        const l = p.lang[language]
        langs[language] = {
            title: l.title,
            group: parent?.lang?.[language]?.title,
            slug: slugify(l.title),
            link: `/${l.href}`,
            body: await tikiToMarkdown(l.body, p.references),
            lastModified: l.lastModified.toISOString()
        }
    }
    return {
        id: p.mainId,
        lang: langs,
        translationNeeded: !p.tags.includes('übersetzt'),
        substanceDone: p.tags.includes('inhaltlich_fertig'),
    }
}

void async function main() {
    console.log('get pages')
    const mapped: {[category: string]: Measure[]} = {}
    const contents: {[idAndLang: string]: string} = {}
    
    const pages = await getPages()

    for(let sectionId in sections) {
        console.log(sections[sectionId])
        let measures: Measure[] = []
        const ps = pages
            .filter(p => p.parentIds.includes(parseInt(sectionId)))

        const ids = []
        
        for(let p of ps) {
            let childrens = pages.filter(pc => pc.parentIds.includes(p.mainId))
            ids.push(p.mainId)
            if(childrens.length) {
                console.log('#'+p.lang.de.title)
                for(let c of childrens) {
                    if(ids.includes(c.mainId)) {
                        // page is duplicated
                        // this happens when page is located on multiple places on the same time
                        continue
                    }
                    if (true) {
                        measures.push(await tikiToMeasure(c, p))

                    }
                }
            } else if (true) {
                // measures which are not in an subsection
                // measures.push(await tikiToMeasure(p))
            }
        }

        // remote contents and add a short teaser instead
        for(let m of measures) {
            for(let langKey in m.lang) {
                contents[m.id+':'+langKey] = m.lang[langKey].body
                m.lang[langKey].teaser = m.lang[langKey].body
                    .slice(0,1000)
                    .replace(/^#+ .*$/mg, '')
                    .replace(/\[(.*?)]\(.*?\)/g, '\$1')
                    .replace(/\\/g, '')
                    .replace(/^\*+/, '')
                    .replace(/\n/mg, ' ')
                    .replace(/__/g, '')
                    .replace(/\*\*/g, '')
                    .replace(/<\/?[^>]+(>|$)/g, "")
                    .trim()
                    .replace(/^- /, '')
                    .slice(0,200)
                delete m.lang[langKey].body
            }
        }
        mapped[sections[sectionId]] = measures
    }


    fs.writeFileSync(path.join(dataDir, 'measures.json'), JSON.stringify(mapped, null, 2), 'utf-8')
    fs.writeFileSync(path.join(dataDir, 'texts.json'), JSON.stringify(contents, null, 2), 'utf-8')

    process.exit()
}()