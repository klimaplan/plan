import * as mysql from 'mysql'
import { spawnPromised, onlyUnique, slugify } from './utils'

let connection: mysql.Connection

async function query(q: string): Promise<Array<{[key: string]: any}>> {
    return new Promise( (resolve, reject) => {
        connection.query(q, function (error, results, fields) {
            if (error) reject(error)
            else resolve(results)
        })
    })
}



export interface PageLanguage {
    id: number
    slug: string
    href: string
    title: string
    body: string
    lastModified: Date
}
export interface Page {
    mainId: number
    parentIds: number[]
    lang: {[language: string]: PageLanguage} 
    tags: string[]
    pos: string,
    references: Reference[]
}
export interface Reference {
    pageId: number
    code: string
    str: string
}



export function connect(conf: mysql.ConnectionConfig) {
    connection = mysql.createConnection(conf)
    connection.connect()
}

// some pages are somehow not connected, and it appears that there are no relations in the database
// ids are now connected here manually
const manualTranslations = {
    269: [354,415],// vorwort
    286: [355,416], // einleitung
    3: [209], // 'energydemocracy'
    4: [205], // 'worapla'
    5: [207], // 'mobility'
    6: [206], // 'erlawa'
    7: [208], // 'reproko'
    8: [210], // 'glogeint'
    353: [386], // nachwort
    281: [369], // glossar
}


export async function getPages(): Promise<Page[]> {

    const pages: Page[] = []
    const translatedPages = await query(`
        SELECT DISTINCT tb.page_id AS translated_page_id,tb2.page_id AS original_page_id
        FROM tiki_pages_translation_bits AS tb
        LEFT JOIN tiki_pages_translation_bits AS tb2 ON tb2.translation_bit_id = tb.source_translation_bit
        WHERE tb2.page_id
    `)
    const manualTranslationsIds = Object.keys(manualTranslations).reduce((o,id) => {o.push(...manualTranslations[id]); return o}, [])
    const mnaualTranslationPages = await query(`
        SELECT p.page_id,p.pageName,p.pageSlug,p.data,p.lastModif,p.lang
        FROM tiki_pages AS p WHERE p.page_id IN (${manualTranslationsIds.join(',')});
    `)

    const pageRes = await query(`
        SELECT p.page_id,p.pageName,p.pageSlug,p.data,p.lastModif,p.lang,GROUP_CONCAT(sp.page_id) AS parent_ids,s.pos
        FROM tiki_pages AS p
        LEFT JOIN tiki_structures AS s ON s.page_id = p.page_id
        LEFT JOIN tiki_structures AS sp ON sp.page_ref_id = s.parent_id
        WHERE (s.structure_id = 1 OR ISNULL(s.structure_id))
        GROUP BY p.page_id
        ORDER BY pos
    `)

    const references: Reference[] = (await query(`
        SELECT page_id,biblio_code,author,title,part,uri,code,year,publisher,location,template
        FROM tiki_page_references
    `)).map(ref => {
        return {
            pageId: ref.page_id,
            code: ref.biblio_code,
            str: ref.template.replace(/~(.*?)~/g, (s,key) => {
                    return ref[key]
                })
                .replace(/<\/?[^>]+(>|$)/g, "")
                .replace(/\(, /, '(')
        }
    })

    const objects = await query(`
        SELECT o.objectId,o.itemId,o.href,GROUP_CONCAT(ft.tag) AS tags FROM tiki_objects AS o
        LEFT JOIN tiki_freetagged_objects AS fto ON fto.objectId = o.objectId
        LEFT JOIN tiki_freetags AS ft ON ft.tagId = fto.tagId
        GROUP BY itemId
    `)

    // main pages
    for(let p of pageRes) {
        if(!p.lang) p.lang = 'de'
        if(!p.parent_ids) continue
        const object = objects.find(t => t.itemId.trim() == p.pageName.trim())
        let page = {
            mainId: p.page_id,
            parentIds: p.parent_ids ? p.parent_ids.split(',').map(a => parseInt(a)).filter(onlyUnique) : [],
            lang: {
                [p.lang]: {
                    id: p.page_id,
                    href: object && object.href,
                    slug: slugify(p.pageSlug),
                    title: p.pageName,
                    body: p.data,
                    lastModified: new Date(p.lastModif*1000)
                }
            },
            tags: [],
            pos: p.pos,
            references: references.filter(r => r.pageId == p.page_id)
        }
        if(object && object.tags) page.tags = object.tags.split(',')
        pages.push(page)
    }
    // add translated pages
    for(let page of pages) {
        const translateIds = translatedPages.filter(t => t.original_page_id == page.mainId).map(t => t.translated_page_id)
        for(let p of pageRes) {

            if(!translateIds.includes(p.page_id)) continue
            if(p.lang == 'en-uk') p.lang = 'en' // use short code
            const object = objects.find(t => t.itemId.trim() == p.pageName.trim())
            page.lang[p.lang] = {
                id: p.page_id,
                href: object.href,
                slug: slugify(p.pageSlug),
                title: p.pageName,
                body: p.data,
                lastModified: new Date(p.lastModif*1000)
            }

            if(object.tags) page.tags.push(...object.tags.split(','))

            const refs = references.filter(r => r.pageId == p.page_id)
            if(refs) page.references.push(...refs)
        }
        if(manualTranslations[page.mainId]) {
            for(let id of manualTranslations[page.mainId]) {
                const tp = mnaualTranslationPages.find(p => p.page_id == id)
                page.lang[tp.lang] = {
                    id: tp.page_id,
                    href: null,
                    slug: slugify(tp.pageSlug),
                    title: tp.pageName,
                    body: tp.data,
                    lastModified: new Date(tp.lastModif*1000)
                }
            }
        }
    }

    // deduplicate tags
    for(let page of pages) {
        page.tags = page.tags.filter((el,i,a) => i===a.indexOf(el))
    }
    return pages
}



let refIndex = 0
let refIndexMap = {}
export async function tikiToLatex(input: string, references: Reference[], downloadFunc: (imageId: string) => Promise<string>): Promise<string> {
    
    input = input.replace(/===(.*?)===/g, '\$1') // remove underline
    input = input.replace(/~~(#[0-9]+|[a-z]+):(.*?)~~/g, '\$2') // remove color
    input = input.replace(/~tc~.*~\/tc~/g, '') // remove comments
    
    input = input.replace(/^[ ]{0,1}---+[ ]{0,1}$/mg, '') // remove seperators

    input = input.replace(/^!!+\s*$/mg, '') // remove empty titles
    input = input.replace(/\{showreference[\s\S]*?\}/g, '') // remove references
    input = input.replace(/\{translationof[\s\S]*?\}/g, '') // remove translation links
    input = input.replace(/^!!+\s*.*(Quellen|Literatur|sources)[\s\S]*$/mg, '') // remove reference section
    input = input.replace(/__([A-Z])/g, '__ \$1') // add space after bold delimiter

    /// remove links to pages
    input = input.replace(/\(\(([^\)]*?)\|([^\)]*?)\)\)/mg, '\$2')
    input = input.replace(/\(\(([\s\S]*?)\)\)/mg, '\$1') 

    // does a heading exist on level 1?
    if(input.match(/^![^!]/gm)) { 
        // shift all headings
        input = input.replace(/^!(.*?)/gm, '!!\$1')
    }
    input = input.replace(/\{img type="fileId" fileId="(\d+)".*?\}/g, 'IMAGEPLACEHOLDER($1)')
    let res = await spawnPromised('pandoc', ['--from=tikiwiki', '--to=latex'], input)

    // res = res.trim()
    // res = res.replace(/\\_/g, '_') // unescape underscores \_
    // res = res.replace(/\{TAG\(\s*tag="(.*?)"\)\}(.*?)\{TAG\}/mg, '<\$1>\$2</\$1>')    
    // // console.log(res)

    // let refsUsed: Reference[] = []
    refIndexMap = {}
    res = res.replace(/\\\{addreference[\s\S]*?biblio\\_code="(.*?)"[\s\S]*?\}/mg, (s,codes) => {
        let ret = ''
        const c = codes.split(',')
        for(let code of c) {
            const ref = references.find(r => r.code == code)
            if(!ref) {
                continue
            }
            const str = ref.str
            .replace(/<\/?[^>]+(>|$)/g, "")
            .replace(/[_%&#]/g, (s) => `\\`+s) // escape characters
            .replace(/(http|https):\/\/[^ "]*/g, (m) => `\\url{${m}}`)

            if(refIndexMap[str]) {
                ret += `\\cref{ref${refIndexMap[str]}}`
            } else {
                refIndexMap[str] = refIndex++
                ret += `\\footnote{\\label{ref${refIndexMap[str]}}${str}}`
            }
        }
        return ret
    })
    // res = res.replace(/\\\{addreference[\s\S]*?biblio\\_code="(.*?)"[\s\S]*?\}/mg, (s,code) => {
    //     code = code.replace(/, /g, '') // wired
    //     const ref = references.find(r => r.code == code)
    //     if(!ref) {
    //         console.log('reference not found', {references, code})
    //         return ''
    //     }
    //     // if(!refsUsed.includes(ref)) refsUsed.push(ref)

    //     // const index = refsUsed.indexOf(ref) + 1
    //     // return `[^${index}]`
    //     const str = ref.str
    //         .replace(/<\/?[^>]+(>|$)/g, "")
    //         .replace(/[_%&#]/g, (s) => `\\`+s) // escape characters
    //         .replace(/(http|https):\/\/[^ "]*/g, (m) => `\\url{${m}}`)

    //     if(refIndexMap[str]) {
    //         return `\\cref{ref${refIndexMap[str]}}`
    //     } else {
    //         refIndexMap[str] = refIndex++
    //         return `\\footnote{\\label{ref${refIndexMap[str]}}${str}}`
    //     }
    // })

    const images = {}
    if(res.match(/IMAGEPLACEHOLDER\((.*?)\)/)) {
        for(let match of res.match(/IMAGEPLACEHOLDER\((.*?)\)/g)) {
            const m = match.match(/IMAGEPLACEHOLDER\((.*?)\)/)
            const id = m[1]
            images[id] = await downloadFunc(id)
        }
        res = res.replace(/IMAGEPLACEHOLDER\((.*?)\)/g, (_,id) => `\\includegraphics[scale=0.3]{${images[id].replace(/[%]/g, (s) => `\\`+s)}}\n\n`)
    }

    // sub script
    res = res.replace(/\\\{TAG\(\s*tag="sub"\)\\\}([\s\S]*?)\\\{TAG\\\}/mg, (s,c) => `\$_${c}\$`)
    res = res.replace(/\{SUB\(\)\}(.*?)\{SUB\}/mg, (s,c) => `\$_${c}\$`)
    res = res.replace(/CO2/g, 'CO\$_2\$')
    res = res.replace(/N2O/g, 'N\$_2\$O')
    res = res.replace(/CH4/g, 'CH\$_4\$')

    // remove some symbols
    // res = res.replace(/→/g, '')

    // empty lines
    res = res.replace(/^~$/mg, '')

    // relative urls (break in pdf)
    res = res.replace(/\{tiki-index\.php/g, '{https://wiki.gerechte1komma5.de/tiki-index.php')

    // remove hypertargets
    // \hypertarget{was-ist-das-problem}{%
    //     \subsubsection{Was ist das~Problem?}\label{was-ist-das-problem}}
    res = res.replace(/\\hypertarget\{.*?\}\{%\s+([\s\S]?)\\label.*/mg, '$1')

    // bold syntax is sometimes not detected
    res = res.replace(/(^| )\\_\\_([\s\S]+?)\\_\\_/mg, ' \\textbf{\$2}')

    return res
}


export async function tikiToMarkdown(input: string, references: Reference[]): Promise<string> {
    input = input.replace(/===(.*?)===/g, '\$1') // remove underline
    input = input.replace(/~~(#[0-9]+|[a-z]+):(.*?)~~/g, '\$2') // remove color
    input = input.replace(/~tc~.*~\/tc~/g, '') // remove comments
    input = input.replace(/~~#.*?:([\s\S]*?)~~/mg, '\$1') //remove color
    
    input = input.replace(/^---+$/mg, '') // remove seperators
    input = input.replace(/^!!+\s*$/mg, '') // remove empty titles


    input = input.replace(/Weiteführende/g, 'Weiterführende') // typo in measure template
    
    // does a heading exist on level 1?
    if(input.match(/^![^!]/gm)) { 
        // shift all headings
        input = input.replace(/^!(.*?)/gm, '!!\$1')
    }
    input = input.replace(/\{img type="fileId" fileId="(\d+)".*?\}/g, 'IMGPLACEHOLDER(https://wiki.gerechte1komma5.de/tiki-download_file.php?fileId=\$1&display)')
    let res = await spawnPromised('pandoc', ['--from=tikiwiki', '--to=gfm'], input)
    res = res.replace(/IMGPLACEHOLDER/g, '![]')
    res = res.trim()
    res = res.replace(/\\_/g, '_') // unescape underscores \_
    res = res.replace(/\{TAG\(\s*tag="(.*?)"\)\}(.*?)\{TAG\}/mg, '<\$1>\$2</\$1>')    
    res = res.replace(/\{SUB\(\)\}(.*?)\{SUB\}/mg, '<sub>\$1</sub>')    

    let refsUsed: Reference[] = []
    res = res.replace(/\{addreference[\s\S]*?biblio_code="(.*?)"[\s\S]*?\}/mg, (s,codes) => {
        let ret = ''
        const c = codes.split(',')
        for(let code of c) {
            const ref = references.find(r => r.code == code)
            if(!ref) {
                // console.log({references, code})
                continue
            }
            if(!refsUsed.includes(ref)) refsUsed.push(ref)
            const index = refsUsed.indexOf(ref) + 1
            ret += `<a href="#fn${index}" class="ref"><sup>[${index}]</sup></a>`
        }
        return ret
    })

    let referenceHTML = ''
    if(references.length) {
        
        referenceHTML += `<section class="footnotes"><ol class="footnotes-list">`

        refsUsed.forEach((ref,i) => {
            referenceHTML += `<li id="fn${i+1}" class="footnote-item">${ref.str.replace(/((http|https):\/\/(.*?))([, ;]|$)/g, (a,url,_,__,end) => `<a href="${url}" target="_blank">${url}</a>${end}`)}</li>`
        })
        for(let ref of references) {
            if(refsUsed.includes(ref)) continue
            referenceHTML += `<li class="footnote-item">${ref.str.replace(/((http|https):\/\/(.*?))([, ;]|$)/g, (a,url,_,__,end) => `<a href="${url}" target="_blank">${url}</a>${end}`)}</li>`
        }
        referenceHTML += `</ol></section>`
    }
    res = res.replace(/CO2/g, 'CO<sub>2</sub>')
    res = res.replace(/N2O/g, 'N<sub>2</sub>O')
    res = res.replace(/CH4/g, 'CH<sub>4</sub>')
    res = res.replace(/\{showreference[\s\S]*?\}/g, referenceHTML)
    res = res.replace(/\n \n/mg, '\n\n')
    return res
}
