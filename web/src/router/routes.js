import { i18n } from '../boot/i18n'
import store from '../data/store'

const routes = [

  // embedded version for gerechte1komma5.de
  {
    path: '/embed/:lang',
    component: () => import('layouts/Embed.vue'),
    beforeEnter: (to, from, next) => {
      i18n.locale = to.params.lang
      next();
    },
    children: [
      {
        path: '/embed/:lang/support',
        component: () => import('pages/Support.vue'),
      },
      {
        path: '/embed/:lang/supporters',
        component: () => import('pages/Supporters.vue'),
      },
    ]
  },


  {
    path: '/:lang',
    component: () => import('layouts/Main.vue'),
    beforeEnter: (to, from, next) => {
      let language = to.params.lang;
      if(!i18n.availableLocales.includes(language)) {
        next('/de/')
        return
      }
      // set the current language for vuex-i18n. note that translation data
      // for the language might need to be loaded first
      // Vue.i18n.set(language);

      i18n.locale = language
      // console.log()
      next();
    },
    children: [
      {
        name: 'support',
        path: '/:lang/support',
        component: () => import('pages/Support.vue'),
      },
      {
        name: 'imprint',
        path: '/:lang/imprint',
        component: () => import('pages/Imprint.vue'),
      },
      {
        name: 'section',
        path: '/:lang/:section',
        component: () => import('pages/Section.vue'),
        beforeEnter: (to,from,next) => {
          store.setSection(to.params.section, to.params.lang)
          next()
        }
      },
      {
        name: 'measure',
        path: '/:lang/:section/:measure',
        component: () => import('pages/Measure.vue'),
        beforeEnter: (to,from,next) => {
          store.setSection(to.params.section, to.params.lang)
          store.setMeasure(to.params.measure, to.params.lang)
          next()
        }
      },
      { name: 'start', path: '', component: () => import('pages/Index.vue') }
    ]
  },
  {
    path: '',
    redirect: () => {
      return {
        name: 'start',
        params: {
          lang: i18n.locale
        }
      }
    },
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}



export default routes
