import Vue from 'vue'
import { i18n } from '../boot/i18n'

import current from './measures.json'
import sectionTexts from './sections.json'
import axios from 'axios'

function sortMeasures(a,b) {
  if(a.substanceDone && !b.substanceDone) return -1
  if(!a.substanceDone && b.substanceDone) return 1
  return 0
}

const store = new Vue({
  data() {
    return {
      sectionKey: null,
      measure: null,
      measureBody: null,
      measureBodyLang: null,
      measureComments: null,
      revisionName: 'current', // TODO,
      supporters: null,
      supportersLoading: false
    }
  },
  computed: {
    lang() {
      return i18n.locale
    },
    sections() {
      return i18n.t('sections')
    },
    sectionKeys() {
      return  [
        "reproko",
        "energydemocracy",
        "mobility",
        "worapla",
        "erlawa",
        "glogeint"
      ]
    },
    section() {
      return i18n.t('sections')[this.sectionKey]
    },
    sectionText() {
      const s = sectionTexts[this.sectionKey]
      if(!s) return ''
      return s[this.lang] || s['en'] || s['de']
    },
    measureLang() {
      if(!this.measure) return null
      return this.measure.lang[this.lang] || this.measure.lang['en'] || this.measure.lang['de']
    },
    measureLangMissing() {
      if(!this.measure) return null
      return !this.measure.lang[this.lang]
    },
    measuresSorted() {
      return current[this.sectionKey].sort(sortMeasures)
    },
    nextMeasure() {
      if(!this.measureLang) return null
      const measures = this.measuresSorted
      const index = measures.findIndex(m => {
        const ml = m.lang[this.lang] || m.lang['en'] || m.lang['de']
        return ml.slug == this.measureLang.slug
      })
      if(index == measures.length-1) return null

      const measure = measures[index+1]
      const ml = measure.lang[this.lang] || measure.lang['en'] || measure.lang['de']
      return {
        title: ml.title,
        slug: ml.slug
      }
    },
    previousMeasure() {
      if(!this.measureLang) return null
      const measures = this.measuresSorted
      const index = measures.findIndex(m => {
        const ml = m.lang[this.lang] || m.lang['en'] || m.lang['de']
        return ml.slug == this.measureLang.slug
      })
      if(index == 0) return null

      const measure = measures[index-1]
      const ml = measure.lang[this.lang] || measure.lang['en'] || measure.lang['de']
      return {
        title: ml.title,
        slug: ml.slug
      }
    },
    measuresList() {
      let res = {}
      for(let sectionKey in this.sections) {
        if(!current[sectionKey]) continue
        res[sectionKey] = current[sectionKey].map(m => {
          const ml = m.lang[this.lang] || m.lang['en'] || m.lang['de']
          return {
            title: ml.title,
            slug: ml.slug,
          }
        })
      }
      return res
    },
    measureListGrouped() {
      let res = {}
      for(let sectionKey in this.sections) {
        const groupMeasures = {}
        let groupLangNames = {}
        if(!current[sectionKey]) continue
        for(let measure of current[sectionKey]) {

          const ml = measure.lang[this.lang] || measure.lang['en'] || measure.lang['de']

          let group = measure.lang['de'].group || '-'
          if(measure.lang[this.lang]) groupLangNames[group] = measure.lang[this.lang].group
          else if(measure.lang['en']) groupLangNames[group] = measure.lang['en'].group

          if(!groupMeasures[group]) groupMeasures[group] = []

          const langAvailable = !!measure.lang[this.lang]
          groupMeasures[group].push({
            title: ml.title,
            slug: ml.slug,
            langAvailable: langAvailable,
            substanceDone: measure.substanceDone,
            translationDone: !measure.translationNeeded,
            reviewed: measure.reviewed,
            teaser: ml.teaser
          })
        }
        res[sectionKey] = {}

        // translate groupnames and sort measures
        for(let group in groupMeasures) {
          let groupName = groupLangNames[group] || group
          res[sectionKey][groupName] = groupMeasures[group].sort(sortMeasures)
        }
      }
      return res
    }
  },
  methods: {
    setSection(slug, lang) {
      const sections = i18n.t('sections')
      for(let key in sections) {
        if(sections[key].slug == slug) {
          this.sectionKey = key
          // this.section = sections[key]
          return
        }
      }
      // for(let key in sections) {
      //   if(sections['en'].slug == slug) {
      //     this.sectionKey = key
      //     // this.section = sections[key]
      //     return
      //   }
      // }
      this.sectionKey = null
    },
    setMeasure(slug, lang) {
      if(!this.section) {
        this.measure = null
        return
      }
      const measures = current[this.sectionKey]
      // for(let m of this.measure[lang])
      for(let measure of measures) {
          const ml = measure.lang[lang] || measure.lang['en'] || measure.lang['de']
          if(ml.slug == slug) {
            if(this.measure !== measure || this.measureBodyLang != lang) {
              this.measure = measure
              this.loadText()
              this.loadComments()
            }
            return
          }
      }
      this.measure = null
    },
    async loadSupporters() {
      if(process.env.SERVER) return
      this.supportersLoading = true
      const res = await axios.get('/api/supporters')
      this.supporters = res.data
      this.supportersLoading = false
    },
    async loadText() {
      let lang
      if(this.measure.lang[this.lang]) lang = this.lang
      if(!lang && this.measure.lang['en']) lang = 'en'
      if(!lang && this.measure.lang['de']) lang = 'de'

      if(this.$ssrTexts) {
        let key = this.measure.id + ':' + lang
        this.measureBody = this.$ssrTexts[key]
      } else {
        this.measureBody = null
        const res = await axios.get(`/api/text/${this.measure.id}/${lang}`)
        this.measureBody = res.data.text
      }
      this.measureBodyLang = lang
    },
    async loadComments() {
      if(process.env.SERVER) return
      this.measureComments = null
      const res = await axios.get(`https://wiki.gerechte1komma5.de/api/comments.php?pageId=${this.measure.id}`)
      this.measureComments = res.data
    }
  }
})

if(!process.env.SERVER) window.store = store
export default store
