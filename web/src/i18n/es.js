export default {
  language: 'Español',
  pagename: 'plan climático desde abajo',
  imprint: 'aviso legal & protección de datos',
  prev: 'volver',
  next: 'siguiente',
  submit: 'enviar',
  startpage: 'Página de inicio',
  downloadPDF: 'Descargar PDF-Versión',
  theSections: 'Las áreas',
  sections: {
    energydemocracy: {
      name: 'Democracia energética',
      slug: 'energydemocracy',
    },
    worapla: {
      name: 'Planificación espacial justa',
      slug: 'worapla'
    },
    mobility: {
      name: 'Justicia de movilidad',
      slug: 'mobility'
    },
    erlawa: {
      name: 'Agricultura justa, soberanía alimentaria y uso de bosques',
      slug: 'erlawa'
    },
    reproko: {
      name: 'Reproducción, producción y consumo justos',
      slug: 'reproko'
    },
    glogeint: {
      name: 'Justicia global y interseccionalidad',
      slug: 'glogeint'
    },
    preface: {
      name: 'Prefacio',
      slug: 'prefacio'
    },
    preamble: {
      name: 'Introducción',
      slug: ''
    },
    epilogue: {
      name: 'Epílogo - Reflexión sobre el proceso de creación',
      slug: 'epilogo'
    },
    glossary: {
      name: 'Glosario',
      slug: 'glosario'
    }
  },
  menu: {
    noMeasures: 'no hay medidas listas en este ámbito',
    kampagneLink: 'a la campaña',
    support: 'apoyar'
  },
  tags: {
    translationNeeded: 'se necesita traducción',
    inProgress: 'contenido en proceso',
  },
  editions: {
    pre: 'Proyecto preliminar en curso',
    first: 'Primera edición',
    second: 'segunda edición'
  },
  section: {
    measures: 'Las medidas',
    supportBy: 'Estas medidas son apoyadas por'
  },
  support: {
    in: {
      question: 'Apoyas al <i>plan climático de abajo</i>?',
      group: 'Sí, como grupo',
      person: 'Sí, como persona',
      nope: 'No, disculpa',
      nopeTitle: 'Que pena!',
      nopeText: 'Qué es que te molesta del plan climático de abajo? escríbenos con gusto a <a href="mailto:klimaplan@gerechte1komma5.de">klimaplan@gerechte1komma5.de</a>',
    },
    group: {
      sectionQuestion: 'Apoyan al plan climático completo o solo a unas medidas',
      name: 'Nombre del grupo/ de la iniciativa',
      mail: 'su correo electrónico',
      website: 'página web (opcional)',
      logo: 'Logo (optional)',
      support: 'Sí, apoyamos al proyecto <i>el plan climático de abajo</i> y nuestro nombre y logo pueden ser mencionados para esto.',
      success: 'Están registrados',
      thanks: 'Grácias que apoyan el plan climático de abajo',
      reset: 'registrar otro grupo'
    },
    person: {
      sectionQuestion: 'Apoyas al plan climático completo o solo a unas medidas',
      firstname: 'Nombre',
      lastname: 'Apellido',
      mail: 'correo electrónico',
      public: 'Si quiero ser nombrado como partidario en la lista pública',
      moreInfos: 'Por favor, envíenme más información sobre cómo puedo unirme a la campaña.',
      thanks: 'Grácias por apoyar al plan climático de abajo',
      reset: 'registrar orta persona'
    },
    fullPlan: 'El <i>plan climático de abajo</i> entero',
    partialPlan: 'un o más ámbitos',
    someData: 'Un par de datos más',
    newsletter: 'Suscrir al boletín de noticias (aproximadamente una vez al mes)',
    pleaseFill: 'Por favor, rellene este campo',
    invalidMail: 'Correo electrónico inválido',
    title: 'Apoye al proyecto<br /><i>el plan climático de abajo</i>!',
    donateNow: '¡dona ahora!',
    text: 'Hasta ahora, todos ellos han sido voluntarios para el <em>plan climático de abajo</em>. No obstante, se producen algunos gastos: Gastos de funcionamiento del sitio web, gastos de viaje para la planificación de reuniones, materiales para los talleres públicos, folletos y mucho más. Todo esto está actualmente pagado por la asociación "für gerechten Klimaschutz e.V.". Desafortunadamente, todavía no hemos podido obtener financiación a largo plazo y por lo tanto dependemos de las donaciones para que el <em>plan climático de abajo</em> pueda convertirse en un proyecto a largo plazo!',
    bankDetails: `<p>
    Las donaciones van a:<br />
    <i>für gerechten Klimaschutz e.V.</i><br>
    IBAN: <span class="monospace">DE24 3545 0000 1101 0991 15</span><br>
    BIC: <span class="monospace">WELADED1MOR</span><br>
    Asunto: gerechte1komma5</p>
    <p>Para obtener un recibo de la donación, por favor, póngase en contacto con <a href="mailto:finanzen@gerechte1komma5.de">finanzen@gerechte1komma5.de</a>`,
    supporters: 'Apoyamos<br /><i>al plan climático de abajo!</i>',
    disclaimer: 'Estos grupos apoyan la idea y los objetivos del plan climático desde abajo. Abogan por que las soluciones existentes se recojan en el conocimiento colectivo del movimiento, con la mayor participación posible de la sociedad. Esto no significa que los grupos estén detrás de todo el contenido de las medidas resumidas en el plan climático de abajo. Esto no es posible porque el plan climático de abajo está en constante desarrollo y crecimiento.'
  },
  measure: {
    langNotAvailable: '<b>Esta medida, desafortunadamente, aún no está disponible en español.</b><p>¿Quizás podrias imaginarte ayudarnos con la traducción?<br />Escríbenos con gusto: <a class="text-white" href="mailto:klimaplan@gerechte1komma5.de">klimaplan@gerechte1komma5.de</a></p>',
    editLink: 'Editar en el Wiki',
    toggleSidebar: 'Mostrar tabla de contenidos y comentarios',
    comments: 'Comentarios',
    commentWrite: 'Escribir un comentario (en el Wiki)',
    noComments: 'Aún no hay comentarios de esta medida. ¡Siéntete libre de escribir el primero!',
    license: 'Esta medida está sujeta a la licencia',
    notFound: 'No se pudo encontrar la medida'
  },
}
