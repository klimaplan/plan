
export default {
  language: 'Deutsch',
  pagename: 'Der Klimaplan von Unten',
  imprint: 'Impressum & Datenschutz',
  prev: 'Zurück',
  next: 'Weiter',
  submit: 'Absenden',
  startpage: 'Startseite',
  downloadPDF: 'PDF-Version downloaden',
  theSections: 'Die Bereiche',

  sections: {
    energydemocracy: {
      name: 'Energiedemokratie',
      slug: 'energiedemokratie',
    },
    worapla: {
      name: 'gerechte Wohn- und Raumplanung',
      slug: 'worapla'
    },
    mobility: {
      name: 'Mobilitätsgerechtigkeit',
      slug: 'mobilitaet'
    },
    erlawa: {
      name: 'Gerechte Landwirtschaft, Ernährungssouveränität und Waldnutzung',
      slug: 'erlawa'
    },
    reproko: {
      name: 'Gerechte Reproduktion, Produktion und Konsumtion',
      slug: 'reproko'
    },
    glogeint: {
      name: 'Globale Gerechtigkeit und Intersektionalität',
      slug: 'glogeint'
    },

    preface: {
      name: 'Vorwort',
      slug: 'vorwort'
    },
    preamble: {
      name: 'Einleitung',
      slug: ''
    },
    epilogue: {
      name: 'Nachwort - Reflexion des Entstehungsprozesses',
      slug: 'nachwort'
    },
    glossary: {
      name: 'Glossar',
      slug: 'glossar'
    }
  },
  menu: {
    noMeasures: 'noch keine fertigen Maßnahmen in diesem Bereich',
    kampagneLink: 'Zur Kampagne',
    support: 'Unterstützen'
  },
  tags: {
    translationNeeded: 'Übersetzungen notwendig',
    inProgress: 'Inhatlich noch in Bearbeitung',
  },
  editions: {
    pre: 'Vorläufiger Entwurf in Bearbeitung',
    first: 'Erste Auflage',
    second: 'Zweite Auflage'
  },
  section: {
    measures: 'Die Maßnahmen',
    supportBy: 'Hinter diesen Maßnahmen stehen'
  },
  support: {
    in: {
      question: 'Stehst du hinter dem <i>Klimaplan von unten</i>?',
      group: 'Ja, als Gruppe',
      person: 'Ja, als Person',
      nope: 'Nein, leider nicht',
      nopeTitle: 'Schade!',
      nopeText: 'Was stört dich denn am Klimaplan? schreib uns gerne an <a href="mailto:klimaplan@gerechte1komma5.de">klimaplan@gerechte1komma5.de</a>',
    },
    group: {
      sectionQuestion: 'Steht ihr hinter dem gesamten Klimaplan oder nur einen Teil der Maßnahmen?',
      name: 'Name der Initative/Gruppe',
      mail: 'Eure Email-Adresse',
      website: 'Webseite (optional)',
      logo: 'Logo (optional)',
      support: 'Ja, wir unterstützen das Projekt <i>der Klimaplan von unten</i> und unser Name und Logo darf dafür genannt werden.',

      success: 'Ihr seid nun eingetragen',
      thanks: 'Danke, dass ihr den Klimaplan unterstützt!',
      reset: 'Weitere Gruppe eintragen'
    },
    person: {
      sectionQuestion: 'Stehst du hinter dem gesamten Klimaplan oder nur einen Teil der Maßnahmen?',
      firstname: 'Dein Vorname',
      lastname: 'Dein Nachname',
      mail: 'Deine Email-Adresse',
      public: 'Ja, ich möchte als Unterstützer*in mit Namen in der öffentlichen Liste stehen.',
      moreInfos: 'Bitte schickt mir weitere Informationen dazu wie ich bei der Kampagne mit machen kann.',

      thanks: 'Danke, dass du den Klimaplan unterstützst!',
      reset: 'Weitere Person eintragen'
    },
    fullPlan: 'Dem gesamten <i>Klimaplan von Unten</i>',
    partialPlan: 'Einen oder mehrere Bereiche',
    someData: 'Noch ein paar Daten',
    newsletter: 'Newsletter abonnieren (ungefähr einmal im Monat)',
    pleaseFill: 'Bitte fülle dieses Feld aus',
    invalidMail: 'Ungültige Email',


    title: 'Unterstütze das Projekt<br /><i>der Klimaplan von unten</i>!',
    donateNow: 'Jetzt spenden!',
    text: 'Bisher engagieren sich alle ehrenamtlich für den <em>Klimaplan von unten</em>. Dennoch fallen einige Kosten an: Betriebskosten für die Website, Fahrtkosten für Planungstreffen, Materialien für die öffentlichen Write-Ins, Flyer und vieles mehr. All dies wird derzeit über Fördergelder vom Verein ‚für gerechten Klimaschutz e.V.‘ bezahlt. Leider konnten wir noch keine langfristigen Förderungen erhalten und sind deshalb auf Spenden angewiesen, damit der <em>Klimaplan von unten</em> ein langfristiges Projekt werden kann!',
    bankDetails: `<p>
    Spenden gehen an:<br />
    <i>für gerechten Klimaschutz e.V.</i><br>
    IBAN: <span class="monospace">DE24 3545 0000 1101 0991 15</span><br>
    BIC: <span class="monospace">WELADED1MOR</span><br>
    Betreff: gerechte1komma5</p>
    <p>Für eine Spendenquittung wenden Sie sich bitte an <a href="mailto:finanzen@gerechte1komma5.de">finanzen@gerechte1komma5.de</a>`,
    supporters: 'Wir unterstützen<br /><i>den Klimaplan von unten!</i>',
    disclaimer: 'Diese Gruppen unterstützen die Idee und Zielsetzung des Klimaplans von unten. Sie treten dafür ein, dass die vorhandenen Lösungen im kollektiven Wissen der Bewegung gesammelt werden, unter möglichst großer Beteiligung der Gesellschaft. Das bedeutet nicht, dass die Gruppen hinter allen Inhalten der im Klimaplan von unten zusammengefassten Maßnahmen stehen. Dies ist schon deshalb nicht möglich, da der Klimaplan von unten sich kontinuierlich weiterentwickelt und wächst.'
  },
  measure: {
    langNotAvailable: '<b>Diese Maßnahme ist leider noch nicht auf Deutsch verfügbar.</b><p>Vielleicht kannst du dir ja vorstellen uns bei der Übersetzung zu helfen?<br />Schreib uns gern: <a class="text-white" href="mailto:klimaplan@gerechte1komma5.de">klimaplan@gerechte1komma5.de</a></p>',
    editLink: 'Bearbeiten im Wiki',
    toggleSidebar: 'Inhaltsverzeichniss und Kommentare einblenden',
    comments: 'Kommentare',
    commentWrite: 'Kommentar schreiben (im Wiki)',
    noComments: 'Bisher noch keine Kommentare zu dieser Maßnahme vorhanden. Schreib gerne den Ersten!',
    license: 'Diese Maßnahme steht unter der Lizenz',
    notFound: 'Maßnahme konnte leider nicht gefunden werden'
  },
}
