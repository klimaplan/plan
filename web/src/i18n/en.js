export default {
  language: 'English',
  pagename: 'grassroots climate plan',
  imprint: 'Imprint & data privacy',
  prev: 'Previous',
  next: 'Next',
  submit: 'Submit',
  startpage: 'Startpage',
  downloadPDF: 'Download PDF Version',
  theSections: 'The Areas',

  sections: {
    energydemocracy: {
      name: 'Energy Democracy',
      slug: 'energydemocracy',
    },
    worapla: {
      name: 'Just Dwelling and Area Planning',
      slug: 'worapla'
    },
    mobility: {
      name: 'Just Mobility',
      slug: 'mobility'
    },
    erlawa: {
      name: 'Just Agriculture, Alimentation Sovereignty and Forest Use',
      slug: 'erlawa'
    },
    reproko: {
      name: 'Just Reproduction, Production and Consumption',
      slug: 'reproco'
    },
    glogeint: {
      name: 'Global Justice and Intersectionality',
      slug: 'glojustint'
    },

    preface: {
      name: 'Preface',
      slug: 'preface'
    },
    preamble: {
      name: 'Preamble',
      slug: ''
    },
    epilogue: {
      name: 'Epilogue - Reflection of the creation process',
      slug: 'epilogue'
    },
    glossary: {
      name: 'Glossary',
      slug: 'glossary'
    }
  },
  menu: {
    noMeasures: 'no measures ready for this section',
    kampagneLink: 'The Campaign',
    support: 'Support'
  },
  tags: {
    translationNeeded: 'Übersetzungen notwendig', // TODO
    inProgress: 'Inhatlich noch in Bearbeitung', // TODO
  },
  editions: {
    pre: 'Work in progress',
    first: 'First edition',
    second: 'Second edition'
  },
  section: {
    measures: 'The measures',
    supportBy: 'These measures are supported by'
  },
  support: {
    in: {
      question: 'Do you support the <i>grassroots climate plan</i>?',
      group: 'Yes, as a group',
      person: 'Yes, as an individual',
      nope: 'No, sorry',
      nopeTitle: 'Too bad!',
      nopeText: 'What is it that you can\'t support in this climate plan? We\'re happy to receive your mail at <a href="mailto:klimaplan@gerechte1komma5.de">klimaplan@gerechte1komma5.de</a>',
    },
    group: {
      sectionQuestion: 'Do you support the whole climate plan or just parts of it?',
      name: 'Name of the group/initiative',
      mail: 'Your email address',
      website: 'Website (optional)',
      logo: 'Logo (optional)',
      support: 'Yes, we support the project <i>grassroots climate plan</i> and our name and logo can be used in that context.',

      success: 'You are on the list now',
      thanks: 'Thank you for your support!',
      reset: 'Add another group'
    },
    person: {
      sectionQuestion: 'Do you support the whole climate plan or just parts of it?',
      firstname: 'Your first name',
      lastname: 'Your last name',
      mail: 'Your email address',
      public: 'Yes, I would like to be visible on the public list of supporters.',
      moreInfos: 'Please send me more information about how I can participate in this campaign.',

      thanks: 'Thank you for your support!',
      reset: 'Add another person'
    },
    fullPlan: 'The whole <i>grassroots climate plan</i>',
    partialPlan: 'One or more sections',
    someData: 'Some more data',
    newsletter: 'Subscribe to our newsletter (ca. once a month)',
    pleaseFill: 'Please fill in this field',
    invalidMail: 'Invalid email',


    title: 'Support the project<br /><i>grassroots climate plan</i>!',
    donateNow: 'Donate now!',
    text: 'So far all the work done on the <em>grassroots climate plan</em> was unpaid. We still have some expenses: Hosting of our website, travel costs for our meetings, material for the public write-ins, flyer and much more. All this is paid by funding from the association ‚für gerechten Klimaschutz e.V.‘. Unfortunately we could not yet acquire any longterm funding, which means that we depend on donations to make the <em>grassroots climate plan</em> reality!',
    bankDetails: `<p>
    Donations for:<br />
    <i>für gerechten Klimaschutz e.V.</i><br>
    IBAN: <span class="monospace">DE24 3545 0000 1101 0991 15</span><br>
    BIC: <span class="monospace">WELADED1MOR</span><br>
    Betreff: gerechte1komma5</p>
    <p>To get a contribution receipt please write to <a href="mailto:finanzen@gerechte1komma5.de">finanzen@gerechte1komma5.de</a>`,
    supporters: 'We support the<br /><i>grassroots climate plan!</i>',
    disclaimer: 'This group supports the idea and the goals of the grassroots climate plan. They want to see the existing solutions getting accumulated in the movement\'s collective knowledge with potentially big parts of society taking part in this endeavor. That does not mean that this group necessarily defends every single statement of the grassroots climate plan and its measures. The grassroots climate plan continously sees changes and amendments, so any support given can only ever be temporary.'
  },
  measure: {
    langNotAvailable: '<b>This measure sadly is not yet available in English.</b><p>Maybe you can imagine helping with translations?<br />Please write to us: <a class="text-white" href="mailto:klimaplan@gerechte1komma5.de">klimaplan@gerechte1komma5.de</a></p>',
    editLink: 'Edit in the wiki',
    toggleSidebar: 'Toggle sidebar',
    comments: 'Comments',
    commentWrite: 'Write comment (in wiki)',
    noComments: 'No comments for this measure so far. You could be first!',
    license: 'This measure is licensed',
    notFound: 'measure not found'
  },
}
