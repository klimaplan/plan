import de from './de'
import en from './en'
import es from './es'

export default {
  de,
  en,
  es
}
