import Vue from 'vue'
import VueI18n from 'vue-i18n'
import messages from 'src/i18n'

Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: 'de',
  fallbackLocale: 'en',
  messages
})


export default ({ app, ssrContext}) => {
  // Set i18n instance on app
  app.i18n = i18n
  if(ssrContext && ssrContext.req.headers['accept-language'] && typeof ssrContext.req.headers['accept-language'] == 'string') {
    const langs = ssrContext.req.headers['accept-language'].split(',')
    for(let l of langs) {
      l = l.split(';')[0]
      if(messages[l]) {
        app.i18n.locale = l
        return
      }
    }
    i18n.locale = 'en'
  } else {
    i18n.locale = 'de'
  }
}

export { i18n }

