/*
 * This file runs in a Node context (it's NOT transpiled by Babel), so use only
 * the ES6 features that are supported by your Node version. https://node.green/
 *
 * All content of this folder will be copied as is to the output folder. So only import:
 *  1. node_modules (and yarn/npm install dependencies -- NOT to devDependecies though)
 *  2. create files in this folder and import only those with the relative path
 *
 * Note: This file is used only for PRODUCTION. It is not picked up while in dev mode.
 *   If you are looking to add common DEV & PROD logic to the express app, then use
 *   "src-ssr/extension.js"
 */

const
  express = require('express'),
  compression = require('compression')

const
  extension = require('./extension'),
  app = express(),
  port = process.env.PORT || 3000

// gzip
app.use(compression({ threshold: 0 }))

app.use((req,res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Content-Type')
  next()
})

// we extend the custom common dev & prod parts here
extension.extendApp({ app })


const server = app.listen(port, () => {
  console.log(`Server listening at port ${port}`)
})

// process.on('uncaughtException', () => {
//   server.close()
// })

process.on('SIGTERM', () => {
  server.close()
})
process.on('exit', () => {
  server.close()
})
