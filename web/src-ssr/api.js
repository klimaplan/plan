const express = require('express')
const router = express.Router()
const db = require('./db')
const axios = require('axios')

const texts = require('../src/data/texts.json')

// const loki = require("lokijs");
const sharp = require('sharp');

const MAILTRAIN_TOKEN = process.env.MAILTRAIN_TOKEN || '-'

async function subscribeNewsletter(email) {
  return axios.post('https://mailtrain.livingutopia.org/api/subscribe/30HzIcpAi?access_token='+MAILTRAIN_TOKEN, {
    EMAIL: email,
    REQUIRE_CONFIRMATION: 'yes'
  })
}
async function subcribeSupportList(email, o) {
  return axios.post('https://mailtrain.livingutopia.org/api/subscribe/AwoZVD_X?access_token='+MAILTRAIN_TOKEN, {
    EMAIL: email,
    REQUIRE_CONFIRMATION: 'no',
    MERGE_NAME: o.name,
    MERGE_TYPE: o.type
  })
}
router.get('/text/:id/:lang', (req,res) => {
  let key = req.params.id + ':' + req.params.lang
  if(texts[key] || texts[key] === '') {
    res.json({
      text: texts[key]
    })
  } else {
    res.status(404).send('Not Found')
  }
})

router.get('/supporters', async (req,res) =>{
  try {
    const groups = await db.getAll('groups')
    const persons = await db.getAll('persons')
    res.json({
      groups: groups.map(g => ({
        name: g.name,
        website: g.website,
        logo: g.logoResized,
        // sections: g.sections
      })),
      persons: persons
        .map(p => {
          if(p.public) return `${p.firstname} ${p.lastname}`
          else return `${p.firstname[0]}‧‧‧‧‧‧ ${p.lastname[0]}‧‧‧‧‧‧`.toLocaleUpperCase()
        }),
    })
  } catch(err) {
    console.error(err)
    res.status(500).send(err.message)
  }
})


router.post('/support/persons', async (req,res) =>{
  try {
    if(!req.body) throw new Error('invalid body')
    if(!req.body.firstname || typeof req.body.firstname !== 'string') throw new Error('firstname is missing')
    if(!req.body.lastname || typeof req.body.lastname !== 'string') throw new Error('lastname is missing')
    if(!req.body.email || typeof req.body.email !== 'string') throw new Error('email is missing')
    // if(!req.body.sections || !Array.isArray(req.body.sections) || !req.body.sections.length) throw new Error('missing or invalid sections array')

    await subcribeSupportList(req.body.email, {
      name: req.body.firstname,
      type: 'person'
    })

    if(req.body.newsletter) {
      await subscribeNewsletter(req.body.email)
      // TODO: add to newsletter
    }
    if(req.body.moreInfos) {
      // TODO: inform orga via mail
    }

    const obj = {
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      email: req.body.email,
      newsletter: !!req.body.newsletter,
      public: !!req.body.public,
      moreInfos: !!req.body.moreInfos,
      // sections: req.body.sections,
      date: new Date
    }

    await db.insert('persons', obj)
    res.json({
      success: true
    })
  } catch(err) {
    console.error(err)
    res.status(500).send(err.message)
  }
})

router.post('/support/groups', async (req,res) =>{
  try {
    if(!req.body) throw new Error('invalid body')
    if(!req.body.name || typeof req.body.name !== 'string') throw new Error('name is missing')
    if(!req.body.email || typeof req.body.email !== 'string') throw new Error('email is missing')
    // if(!req.body.sections || !Array.isArray(req.body.sections) || !req.body.sections.length) throw new Error('missing or invalid sections array')

    await subcribeSupportList(req.body.email, {
      name: req.body.name,
      type: 'group'
    })

    if(req.body.newsletter) {
      // TODO: add to newsletter
    }
    const obj = {
      name: req.body.name,
      email: req.body.email,
      website: req.body.website,
      logo: null,
      logoResized: null,
      newsletter: !!req.body.newsletter,
      // sections: req.body.sections,
      date: new Date
    }

    if(req.body.logo) {
      const orig = req.body.logo
      const o = orig.match(/^data:([a-z0-9\-]+\/[a-z0-9\-]+);base64,(.*)/)
      if(!o) throw new Error('invalid file')
      // const mime = o[1]

      obj.logo = orig
      const buf = Buffer.from(o[2], 'base64')
      const image = sharp(buf)
      // const metadata = await image.metadata()

      const res = await image
        .resize({
          width: 150,
          height: 90,
          fit: sharp.fit.contain,
          background: {r:0,g:0,b:0,alpha:0}
        })
        .toFormat('png')
        .toBuffer()
      obj.logoResized = res.toString('base64')
    }
    await db.insert('groups', obj)
    res.json({
      success: true
    })
  } catch(err) {
    console.error(err)
    res.status(500).send(err.message)
  }
})


module.exports = router;
