const fs = require('fs').promises
const path = require('path')
let data = null
const filename = path.join(require('os').homedir(), 'klimaplan-db.json')

module.exports = db = {

  async load() {
    try {
      data = JSON.parse(await fs.readFile(filename, 'utf-8'))
    } catch(err) {
      if(err.code == 'ENOENT') {
        data = {}
      } else {
        throw err
      }
    }
  },
  async save() {
    await fs.writeFile(filename, JSON.stringify(data, null, 2), 'utf-8')
  },
  async getAll(key) {
    if(!data) {
      await db.load()
    }
    return data[key] || []
  },
  async insert(key, obj) {
    if(!data) {
      await db.load()
    }
    if(!data[key]) data[key] = []
    data[key].push(obj)
    await db.save()
  }
}
