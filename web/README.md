# Der Klimaplan von Unten - Veröffentlichungs-Webseite

## Download the repository
```bash
git clone git@gitlab.com:klimaplan/plan.git
cd plan
```

## Install the dependencies
```bash
yarn
```

### Start the app in development mode
```bash
yarn dev
```
