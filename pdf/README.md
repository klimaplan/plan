
## LaTeX Ebene


| latex Ebene     | genutzt für    |
| ----------------|----------------|
| `\chapter`      | Bereich        |
| `\section`      | Unterbereich   |
| `\subsection`   | Maßnahme       |
| `\subsubsection`| Fragen
| `\paragraph`    | 
| `\subparagraph` |  
