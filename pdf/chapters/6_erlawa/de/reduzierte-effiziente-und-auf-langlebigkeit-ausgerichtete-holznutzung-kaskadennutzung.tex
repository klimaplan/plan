\subsection{Reduzierte, effiziente und auf Langlebigkeit ausgerichtete Holznutzung (Kaskadennutzung)}
\hypertarget{was-ist-das-problem}{%
\subsubsection{Was ist das~Problem?}\label{was-ist-das-problem}}

Wenn Holz stofflich genutzt wird, so bleibt der durch den Bau gebundene
Kohlenstoff vorerst im Holzprodukt gespeichert. Vor diesem Hintergrund
wird die Dauer der Nutzung des Holzproduktes relevant. Der hohe Anteil
der energetischen Verwertung wird so aus Klimaperspektive zum Problem.
Weltweit werden 2,3 Milliarden Kubikmeter und damit 59\% des Holzes zur
Energiegewinnung als Feuerholz genutzt und 41\% oder 1,6 Milliarden
Kubikmeter für andere stoffliche Nutzung. Die energetische Nutzung in
diesem großen Maßstab muss zurückgefahren und gerade bezüglich der
Anwendung zum Kochen und Heizen durch erneuerbare Energien ersetzt
werden, da sonst der gebundene Kohlenstoff direkt wieder frei wird. Ziel
muss es sein, die energetische Nutzung erst ans Ende einer
Nutzungskaskade zu stellen.

Momentan wird viel Holz (als Rohmaterial oder in verarbeiteter Form)
exportiert und importiert. Die damit verbundenen Transportwege und
Handelsströme verursachen Emissionen. Außerdem zeigt sich mit Blick auf
die Import- und Exportstatistiken ein typisch postkoloniales Bild.
Wertvolle Rohstoffe (Holz aus Urwäldern etc. ) werden aus Ländern des →
\href{https://wiki.gerechte1komma5.de/tiki-index.php?page=Glossar\&amp;structure=Allgemeines\&amp;latest=1\#GLOBALER_S_DEN}{globalen
Südens} zur Verarbeitung nach Europa geschafft, wo die letztendliche
Wertsteigerung stattfindet. Das Produkt wird dann größtenteils dort
verwendet oder in kleineren Mengen wieder in die Herkunftsländer zurück
verkauft. So sind die Netto-Waldverluste fast ausschließlich auf Länder
des globalen Südens verteilt und auf dem Afrikanischen Kontinent ähnlich
präsent wie auf dem Mittel- und Südamerikanischen, sowie in Südostasien.
Die Rohstoffverfügbarkeit ist dabei ein globales Problem welches
deutlich die nach wie vor postkolonialen Machtstrukturen zeigt.

Die anzustrebende regionale Holzverarbeitung wird immer schwieriger, da
traditionelle Sägemühlen nicht mehr rentabel sind oder von Großbetrieben
vom Markt verdrängt wurden. Auch hier manifestiert sich eine global
ungleiches Machtgefüge zwischen globalem Süden und Norden. Hinzu kommt
der massive Einfluss der Industrien auf die angepflanzten Spezies. So
wächst Nadelholz nicht nur schneller, sondern ist gerade in der
stofflichen Verwertung auch mit besseren Eigenschaften ausgestattet. Aus
ökologischer Perspektive ist einer einseitigen Ausrichtung jedoch eine
klare Absage zu erteilen. Die Ergebnisse einer solchen Waldpolitik sind
weltweit, in Deutschland zum Beispiel anhand der besonders stark unter
Druck stehenden →
\href{https://wiki.gerechte1komma5.de/tiki-index.php?page=Glossar\&highlight=monokulturen\#KOSYSTEM}{Ökosysteme}
abzulesen in denen Fichten und Kiefermonokulturen dominant sind.

Es muss ein Umdenken sowohl in der stofflichen Verwertung, als auch im
Verbrauch generell stattfinden, Stichwort →
\href{https://wiki.gerechte1komma5.de/tiki-index.php?page=Glossar\&highlight=monokulturen\#SUFFIZIENZ}{Suffizienz}.
So hat sich in den letzten 30 Jahren der Holzverbrauch in Deutschland
verdoppelt, was gerade aufgrund seiner Eigenschaft als Erneuerbare
Ressource auch im Sinne des Klimas sein kann. Schließlich ist die
zukünftig gesteigerte Nutzung von Holz aus Baustoff schon aufgrund des
vergleichsweise geringen Energieaufwandes höchst sinnvoll. Das heißt im
Umkehrschluss, wir brauchen Holz um eine klimaneutrale Welt zu
gestalten, aber nicht in den Händen einer konsum- und profitorientierten
Wirtschaft, sondern ausgerichtet nach dem tatsächlichen Gebrauch der
weltweiten Zivilgesellschaft.

\hypertarget{was-ist-die-mauxdfnahme}{%
\subsubsection{Was ist die~Maßnahme?}\label{was-ist-die-mauxdfnahme}}

In \textbf{langlebigen Produkten} wie (lange genutzten) Möbeln oder Holz
als Baustoff ist die langfristige Bindung des Kohlenstoffs gegeben.
Produkte, welche aus Primärholz hergestellt werden, müssen also für eine
langlebige Nutzung konzipiert werden. Werden weniger Produkte - dafür
über einen längeren Zeitraum - genutzt, steigt selbst bei steigender
Verwendung von Holz beispielsweise als Baumaterial der Holzverbrauch
nicht an, sondern kann im Gegenteil sinken. Holz als \textbf{stoffliches
Substitutionsmaterial} ist durchaus relevant, insbesondere da es andere
nicht-erneuerbare und oft energieintensive Materialien (z.B. Zement)
ersetzen kann. Neben der effizienten und langlebigen Holznutzung ist die
 \textbf{ Kaskadennutzung} relevant. Dabei wird Holz mehrfach genutzt und
auf verschiedenen Stufen für verschiedene Nutzungsbereiche eingesetzt.

Anders als die stoffliche Substitution ist die energetische Verwendung
(als Substitution für fossile Energieträger) kritisch zu sehen. Bei der
Verwendung von Holz (in Form von Pellets oder Hackschnitzeln) zur
Erzeugung von Wärme und Strom wird zum einen der vormals im Holz
gespeicherte Kohlenstoff wieder frei. Im sehr kleinen Stil für die
Nutzung von nicht anders verwendbaren Holzabfällen ist die energetische
Nutzung von Holz eine Möglichkeit, allerdings darf diese Form der
Nutzung nicht als großskaliges Geschäftsmodell aufgezogen werden.
Insbesondere Import von nicht nachhaltig geschlagenem Holz in Form von
Hackschnitzeln oder Pellets ist hoch problematisch.

\hypertarget{wie-kann-die-umsetzung-aussehen}{%
\subsubsection{Wie kann die
Umsetzung~aussehen?}\label{wie-kann-die-umsetzung-aussehen}}

\begin{itemize}
\tightlist
\item
  Es braucht mehr lokale und regionale Wertschöpfungsketten für eine
  Verarbeitung von Holz als Baustoff. Kleine, lokale und regionale
  Sägewerke, welche den Rohstoff zum Bau/Werkstoff verarbeiten, sollten
  durch Fördermaßnahmen unterstützt werden.
\item
  Es werden Architekt*innen und Planer*innen, die dem Baustoff Holz den
  Vorrang geben, sowie Zimmerleute, die mit dem Baustoff Holz langlebige
  und haltbare Gebäude errichten, und Tischler*innen und Schreiner*innen
  die mit dem Werkstoff Holz langlebige und haltbare Möbel und Fußböden
  bauen können, benötigt. Eine Fokussierung hierauf in den jeweiligen
  Ausbildungen der Berufe ist damit erforderlich.
\item
  Politische Maßnahmen, zum Beispiel Bauvorschriften, die dem Baustoff
  Holz den Vorrang geben. Generell Gesetze, welche die ökologische
  Bilanz von Produkten insbesondere Baustoffen berücksichtigt.
\item
  Für Produkte aus Holz können Vorschriften hinsichtlich einer
  Mindestnutzungsdauer festgelegt werden, sowie eine Verpflichtung, die
  Nutzbarkeit der Gegenstände über entsprechend lange Zeiträume zu
  garantieren.
\item
  Viele Bauprojekte werden durch staatliche Entscheidungsträger*innen in
  Auftrag gegeben. Zudem macht die Nutzung von Holzprodukten (Papier!)
  im öffentlichen Raum einen großen Anteil aus. Hier muss vom Primat der
  günstigsten Beschaffung abgerückt werden hin zu der Auswahl anhand von
  ökologischen und sozialen Standards.
\item
  In manchen Bereichen (bspw. Hygieneartikel, welche notwendigerweise
  das Ende der Nutzungskaskade darstellen, aber auch Papier/Pappe/viele
  Verpackungsprodukte) könnte die Verwendung von Primärfasern
  schlichtweg verboten werden und eine Pflicht zur Nutzung von
  recycelten Materialen eingeführt werden.
\end{itemize}

→
\href{https://wiki.gerechte1komma5.de/tiki-index.php?page=Glossar\&highlight=monokulturen\#SUFFIZIENZ}{Suffizienz}
muss auch in diesem Wirtschaftszweig als oberstes Gebot gelten um gerade
den übermäßigen und oftmals sinnlosen Konsum von Produkten einzudämmen.
Eine Abkehr von der →
\href{https://wiki.gerechte1komma5.de/tiki-index.php?page=Glossar\#EFFIZIENZ}{Effizienz}
ist also auch hier geboten. Es darf nicht zu einer 1:1-Substitution von
Rohstoffen unserer heutigen Wirtschaft kommen, da diese schlicht
rechnerisch nicht möglich ist. Um ein Vorgehen auf gesellschaftlicher
Ebene zu verhandeln ist auch die Wissenschaft und ihre Förderung
gefragt, um Zahlen und einen Maßstab zu liefern.

\hypertarget{wie-wird-damit-dem-klimawandel-entgegen-gewirkt}{%
\subsubsection{Wie wird damit dem Klimawandel
entgegen~gewirkt?}\label{wie-wird-damit-dem-klimawandel-entgegen-gewirkt}}

Wälder und Produkte aus Holz sind an sich Kohlenstoffsenken. Zudem
reduziert die effizientere Nutzung von Holz und die Nutzung in Kaskaden
den Gesamtholzverbrauch bzw. Holz kann an andere Stellen THG-intensive
und problematische Stoffe (z.B. im Bau) \textbf{ersetzen}, wodurch
erhebliche Mengen an THG eingespart werden können. Auch eine
Regionalisierung wirkt durch das \textbf{ Vermeiden} von beim Transport
anfallenden Emissionen dem Klimawandel entgegen.

\hypertarget{welche-anderen-effekte-hat-die-mauxdfnahme}{%
\subsubsection{Welche anderen Effekte hat
die~Maßnahme?}\label{welche-anderen-effekte-hat-die-mauxdfnahme}}

Weitere sich ergebende positive Effekte sind die Stärkung des ländlichen
Raums durch verarbeitende Wirtschaftszweige direkt vor Ort. Durch den
insgesamt nicht steigenden, gegebenenfalls sogar sinkenden Verbrauch (je
nachdem in welchem Ausmaß andere Baustoffe substituiert werden und je
nachdem wie stark der Gesamtverbrauch reduziert wird) kann auch
insgesamt der Waldanteil, welcher nicht stark wirtschaftlich genutzt
wird, erhöht werden, was positiv auf die →
\href{https://wiki.gerechte1komma5.de/tiki-index.php?page=Glossar\#BIODIVERSIT_T}{Biodiversität}
und →
\href{https://wiki.gerechte1komma5.de/tiki-index.php?page=Glossar\&highlight=monokulturen\#RESILIENZ}{Resilienz}
der Waldökosysteme rückwirkt. Auch könnte im Zuge der Ausrichtung auf
langlebige Holzprodukte der Laubholzanteil, insbesondere Buchen und
Eichen, welche auch natürlicherweise in Deutschland vorkommen, erhöht
werden.

\hypertarget{wie-lang-dauert-es-bis-die-mauxdfnahme-wirkung-zeigt}{%
\subsubsection{Wie lang dauert es, bis die Maßnahme
Wirkung~zeigt?}\label{wie-lang-dauert-es-bis-die-mauxdfnahme-wirkung-zeigt}}

Wirkungen durch langlebigere Nutzung von Holzprodukten oder die Nutzung
in Kaskaden zeigen sofortige Effekte durch die Reduktion des
Holzeinschlags oder der Holzimporte bzw. den Rückgang der Verwendung von
beispielsweise Zement. Mit der Maßnahme einhergehende Umgestaltungen von
Berufen und Produktionsketten benötigen etwas Zeit, aber die Maßnahme
selbst zeigt mit der beginnenden Umsetzung eine Wirkung.

\hypertarget{bezuxfcge-zu-sozialer-globaler-oder-generationengerechtigkeit}{%
\subsubsection{Bezüge zu sozialer, globaler
oder~Generationengerechtigkeit}\label{bezuxfcge-zu-sozialer-globaler-oder-generationengerechtigkeit}}

Insbesondere die beiden Aspekte der Regionalisierung der Holznutzung und
der stofflichen Substitution durch heimisches Holz gehen mit sehr
positiven Auswirkungen für die globale Gerechtigkeit einher. Durch
Importe von Holz oder Holzprodukten wird momentan die Rodung und
Zerstörung von Wald in andere Länder ausgelagert und oft dadurch
indirekt eine nicht nachhaltige Waldnutzung unterstützt. Besonders
relevant ist hierbei die Zerstörung von besonders schützenswerten
Primärwäldern und die Auswirkungen auf die lokale, vom Wald lebenden und
auf intakte Wälder angewiesene Bevölkerung. Erhebliche positive
Auswirkungen sind zudem mit der Substitution von hochproblematischen
Stoffen wie Zement oder Beton verbunden. Mit dem Extraktivismus (Abbau
und Gewinnung) von z.B. Zement sind massive Beeinträchtigungen von
Ökosystemen (oft sehr sensible Ökosysteme wie beispielsweise
Karstgebiete), Umsiedlungen und Menschenrechtsverletzungen der lokalen
Bevölkerung verbunden. Zudem erfolgt der Abbau in der Regel durch
transnational agierende Großkonzerne (Beispiel: Zementabbau in
Indonesien) Auch die Stärkung von lokalen und regionalen
Wertschöpfungsketten und handwerklichen Berufen trägt zu sozialer
Gerechtigkeit und der Stärkung des ländlichen Raums bei.
