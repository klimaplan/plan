\subsection{Protection and rewetting of moor soils}
\hypertarget{whats-the-measure}{%
\subsubsection{What's the~measure?}\label{whats-the-measure}}

Deconstruction of the drainage of moor~soils. The agricultural use of
drained moor soils must be stopped or converted to
\href{https://wiki.gerechte1komma5.de/tiki-index.php?page=Glossary\&no_bl=y\#PALUDICULTURES}{paludiculture}.

\hypertarget{introduction}{%
\subsubsection{Introduction}\label{introduction}}

Moors are landscapes in which dead, only partially decomposed plant
rests~have accumulated as "peat" due to permanent water saturation of
the soil. Because the plant rests~consist of 50-60\% carbon, moors
contain the highest concentration of carbon of all terrestrial
\href{https://wiki.gerechte1komma5.de/tiki-index.php?page=Glossary\&no_bl=y\#ECOSYSTEM}{ecosystems}.
The vast majority of Germany's moors are now drained - with increasingly
obvious negative consequences. Drainage causes oxygen to enter the soil,
the peat is microbially decomposed, large amounts of
\href{https://wiki.gerechte1komma5.de/tiki-index.php?page=Glossary\&no_bl=y\#GREENHOUSE_GAS_EMISSIONS}{greenhouse
gases} (GHG; CO$_2$ and N$_2$O) and nutrients are released and the moor loses
1-2 cm of height every year, leading to increasing drainage costs, flood
risks and ultimately land loss.

\hypertarget{how-does-this-save-co2eq-how-much}{%
\subsubsection{How does this save Co2eq
(/How~much)?}\label{how-does-this-save-co2eq-how-much}}

There are three main ways in which climate-friendly bog management can
save greenhouse gas (GHG) emissions or even fix carbon in the soil:

\begin{enumerate}
\tightlist
\item
  \textbf{ Avoiding carbon losses~(avoidance):} By rewetting, i.e.
  closing existing drainage systems, GHG emissions from drained moors
  are greatly reduced.
\item
  \textbf{ Binding \& using~carbon (biofuels/bioresources):} If, after
  rewetting, the growing biomass is used to replace fossil raw materials
  and energy sources, an additional reduction in emissions is achieved
  compared to abandonment of use. This reduction can amount to 4-10 t CO
  2 eq. per ha and year (Dahms et al. 2017).
\item
  \textbf{ Carbon capture \& storage~(carbon capture \& storage):~}
  Through rewetting, moorlands can grow again and permanently fix part
  of the produced biomass as peat. The annual
  \href{https://wiki.gerechte1komma5.de/tiki-index.php?page=Glossary\&no_bl=y\#SINK}{sink}
  capacity is not high (about one t CO 2 eq. per ha and year), but
  certainly - in the absence of alternative, long-term effective sinks -
  significant
\end{enumerate}

~ The 17,800 km² of drained, mainly agriculturally used peatlands in
Germany produce 51 million tonnes of CO$_2$ eq., or 5.7 \% of total German
greenhouse gas emissions. Meadows and pastures on drained moors emit 29
tons per hectare per year, arable land even 37 tons.³ This could be
saved if the moor soil drainage was reduced. In this way, 20-30 tons of
\href{https://wiki.gerechte1komma5.de/tiki-index.php?page=Glossary\&no_bl=y\#CO$_2$_EQUIVALENT}{CO$_2$-eq}.
per hectare could be saved annually.¹² Re-wetted moor~areas emit hardly
any CO$_2$ and nitrous oxide. Although methane emissions can occur, methane
is much more short-lived in the atmosphere than other gases and
contributes much less to long-term warming.

Rapid rewetting (between 2020 and 2040; rewet all, start now) leads to
faster netto-emission-reductions and a significantly lower warming
effect triggered by peatlands than rewetting that only takes place
between 2050 and 2070 (rewet all, start later).~

\includegraphics[scale=0.3]{images/Abb_2.1.png}

Fig. 2.1 Default values for annual greenhouse gas
emissions from peat soils in Germany (in tonnes of CO 2 equivalents per
hectare) for various forms of use (according to Joosten et al. 2016,
based on values of the Intergovernmental Panel on Climate Change IPCC)

\includegraphics[scale=0.3]{images/rewet_strategies.png}



Fig. 3:~ Projected radiative forcing (mW/m 2 ) and temperature effect of
greenhouse gas emissions from peatlands worldwide in the period
2000-2100¹¹. The total human-made radiative forcing in the period 1750
to 2011 was 2.3 W/m2 net (i.e. after deduction of cooling effects) (IPCC
AR5).

\includegraphics[scale=0.3]{images/Tab_3.1.png}



\hypertarget{how-long-does-it-take-the-measure-to-become-effective}{%
\subsubsection{How long does it take the measure to
become~effective?}\label{how-long-does-it-take-the-measure-to-become-effective}}

While CO$_2$ emissions are immediately reduced sharply as the water level
rises to the surface, emissions of CH-4 (methane) increase. In the first
few years after rewetting, it is often even higher than in natural
moors, especially when they are flooded. Since CH$_4$ has 34 times the
global warming potential of CO$_2$, the climate impact of a rewetted moor
is often slightly negative. However, the negative climate impact is
considerably reduced compared to the previous drained state (Joosten et
al. 2016). As soon as after 5-10 years a closed, at best peat-forming
vegetation cover has formed, the emissions of a rewetted moor~are
similar to those of a natural moor~(Fig. 2.1).

The following measures reduce the CH$_4$ emissions caused by rewetting:

\begin{itemize}
\tightlist
\item
  Removal of above-ground biomass before rewetting;
\item
  Removal of 5-10 cm of topsoil before rewetting to remove the
  underground biomass and reduce the nutrient availability in the soil;
\item
  Avoidance of overflow and open water areas (also in ditches);
\item
  Use of water that is as low in nutrients as possible;
\item
  Gradual, stepwise raising of the water level;
\item
  Facilitation of plant species typical of the bog.
\end{itemize}

\hypertarget{other-positive-effects}{%
\subsubsection{Other positive~effects}\label{other-positive-effects}}

~ Moor climate protection¹² as~nature-based solution is:

\begin{itemize}
\tightlist
\item
  Tried and tested (federal states such as Mecklenburg-Western Pomerania
  can quantify the savings achieved)
\item
  Cost-efficient (with one-off planning and construction costs of around
  4000 euros per hectare, 20-30 tonnes of CO$_2$ eq. per hectare could be
  saved annually)
\item
  Synergetic (through water and nutrient retention, flood protection,
  landscape cooling and promotion of
  \href{https://wiki.gerechte1komma5.de/tiki-index.php?page=Glossary\&no_bl=y\#BIODIVERSITY}{biodiversity}
\item
  Area-neutral (in the case of paludiculture) or area-favourable (in the
  case of abandonment of use)
\end{itemize}



\hypertarget{how-can-this-be-implemented}{%
\subsubsection{How can this
be~implemented?}\label{how-can-this-be-implemented}}

A transformation path¹² for the future climate-friendly management of
peatlands should be developed today, with clear goals (net zero
emissions, which can only be achieved by additional C-regulation) and
milestones, which will give all actors long-term planning security. We
propose the following transformation path:

\begin{itemize}
\tightlist
\item
  Forest: 50\% of drained forest should be rewetted by 2030, an
  additional 25\% by 2040 and the remaining 25\% by 2050;
\item
  Arable land: phasing out arable land use on peaty soils by 2030,
  conversion of arable land into grassland with substantially raised
  water levels (see grassland) or paludiculture;
\item
  Grassland: water level increase on all grassland up to ≤ 30 cm below
  ground and on at least 200,000 ha (15 \%) in pasture by 2030. Stop
  support for increased drainage
\item
  Raising water levels in arable land to 60\% of total grassland by 2040
  and to 100\% of the area by 2050
\item
  peat extraction: phasing out peat extraction and consumption and
  replacing all peat with renewable alternatives by 2030;
\item
  other wetlands (unused areas): achieving net zero emissions (CO$_2$) by
  2030;
\item
  settlements: Re-wetting of two thirds of the settlement area on
  drained peatlands by 2050.
\end{itemize}

The following instruments and measures can be used to achieve the
objectives:

\begin{itemize}
\tightlist
\item
  Stopping agricultural
  \href{https://wiki.gerechte1komma5.de/tiki-index.php?page=Glossary\&no_bl=y\#SUBSIDIARITY}{subsidies}
  for arable land on drained moor soils from 2021, phasing out arable
  land use on moor
\item
  Recognition of paludiculture as agriculture and inclusion in
  agricultural
  subsidies,\href{https://wiki.gerechte1komma5.de/tiki-index.php?page=Glossary\&no_bl=y\#INVESTMENTS}{
  investment} programmes and climate protection area premium
\item
  Stop the drainage of all federally owned bog areas by 2030 and
  establish paludiculture demonstration farms~
\end{itemize}

\includegraphics[scale=0.3]{images/Abb_4.png}



Figure 4:~ Development trajectories and intermediate targets for the
area shares of the individual land use categories on peat soils in the
LULUCF sector according to the transformation path 2050.~ Dry = deeply
drained (peat-consuming); Moist = slightly drained (water pond
\textasciitilde30 cm below ground level, peat-consuming); Wet = water
pond in ground level (peat-conserving).
