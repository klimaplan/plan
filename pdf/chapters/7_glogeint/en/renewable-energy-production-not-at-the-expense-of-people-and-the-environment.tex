\subsection{Renewable energy production not at the expense of people and the environment}
\hypertarget{what-is-the-problem}{%
\subsubsection{What is the problem?}\label{what-is-the-problem}}

The energy sector is the largest contributor to climate change,
accounting for 25\% of global greenhouse gas emissions. Therefore, the
switch from fossil fuels to renewable energies is essential to achieve
the objectives of the Paris Agreement. However, a closer look reveals
that many renewable energies have disastrous social as well as
environmental consequences. A few examples of renewable energy
production projects that are being implemented in the global South and
that have the negative consequences mentioned above will be presented.

\hypertarget{hydpropower-projects}{%
\paragraph{Hydpropower projects}\label{hydpropower-projects}}

Hydropower is experiencing a boom worldwide in the form of huge dams.
Many governments see them as a necessary and important technology for
the energy revolution. The International Hydropower Association(IHA) is
an organization of companies and investors from all over the world, who
are active in the hydropower business. At the IHA Congress, which took
place in May 2017 in Addis Ababa, many discussions related to both the
UN goals for sustainable development (SDG) and the Paris Climate
Agreements. Both include a commitment to improve access to electricity
and water in the context of climate change, and the hydropower industry
is eager to present its technology as a solution to both problems.
\footnote{\label{ref4} Lateinamerika Nachrichten und Gegenstroemung : GESTAUTE STRÖMEWASSERKRAFT: FLUCH ODER SEGEN FÜR LATEINAMERIKA?  (2017, ) \url{https://lateinamerika-nachrichten.de/wp-content/uploads/2017/07/PDF-Dossier-Wasserkraft-LN517\_518\_WEB.pdf}}

In many cases, however, hydropower projects lead to massive human rights
violations: People are displaced, lose their arable land and thus their
access to sufficient food. In addition, studies show that large-scale
damming of rivers is more harmful to the climate than beneficial. The
flooded plants rot. In some cases, this produces as many greenhouse
gases as burning fossil energy.

\hypertarget{hydropower-in-brasil}{%
\subparagraph{Hydropower in Brasil}\label{hydropower-in-brasil}}

The Belo Monte hydropower plant is an important infrastructure project
of the government. It is intended to bring development to the region,
improve the country's energy security and contribute to climate
protection. The local population is affected in various ways by the
construction of the dams: Housing settlements are flooded and fishing
grounds destroyed, an important source of livelihood for the local
population (which includes various indigenous groups). There was also no
consultation and compensation as requiered by ILO Convention 169. Many
indigenous communities have already taken legal action against the
project. Finally, the electricity produced is not destined for the
surrounding villages (where many people live without electricity), but
is channelled to the urban economic centres of Brazil.

With regard to the hydropower projects, the parties involved are
motivated not only by energy production but also by the need to create
cost-effective transport routes for raw materials. For example, the
Tapajós dam project will make it worthwhile to grow soy on a large scale
in regions that have so far been spared the expansion of the
agricultural industry. In the case of the Belo Monte dam project,
several European companies are involved, including German companies,
including Siemens and Voith Hydro. Critics report that these companies
are not fulfilling their human rights responsibilities sufficiently.
Insufficient checks on the consequences of the project were carried.
Also no discreet discussions with the protesting population was
realized. \cref{ref4} \footnote{\label{ref5}Kleiber, Tina und Christian Russau (2014): Der Belo-Monte-Staudamm und die Rolle europäischer Konzerne, Berlin: Gegenstroemung, }

\hypertarget{battery-production-lithium-mining}{%
\paragraph{Battery production: Lithium
mining}\label{battery-production-lithium-mining}}

Lithium has become an enormously sought-after raw material due to the
switch to electromobility and the prioritization of battery operation.
However, lithium mining is problematic in regard of its high water
consumption and other impacts on the surrounding ecosystems. Chile is
the world's second largest producer of lithium and by far the largest
exporter of lithium carbonate. The example of the Atacama salt lake
already clearly shows how much the environment suffers when lithium is
extracted. According to a recent study on the effects of lithium in the
Atacama salt lake, lithium production alone requires about 200 million
liters of water per day. This damages bird species and microfauna and
reduces the available water for the neighboring population. Dust from
the slag heaps spreads in the environment and can damage the health of
people and animals. Because of the current lithium boom, the Chilean
government has approved a tripling of production volumes at the
beginning of 2018. The surrounding communities, which were not included
in this decision, do not see themselves participating in the profits and
instead fear for their water supply. \footnote{\label{ref6}Powershift:  Neue Rohstoffkapitel in EU-Handelsabkommen – eine Bestandsaufnahme (2019, ) \url{https://power-shift.de/wirtschaftsinteressen-vor-umwelt-und-menschenrechten/}}

\hypertarget{wind-power-solar-plants-and-geothermal-energy}{%
\paragraph{Wind power, solar plants and geothermal
energy}\label{wind-power-solar-plants-and-geothermal-energy}}

Against the background of climate-damaging dams, wind turbines and solar
plants are all the more important. However, their production requires
mineral raw materials that are mined in Central Africa and South
America. For these mines, too, people are often forced to relocate or
have to work under life-threatening conditions. Resettlement measures
are also carried out for geothermal energy plants, where the population
is not always compensated. For example, a group of Masai in Olkaria
(Kenya) was relocated for such a project, but they are still waiting for
the allocation of their own land rights in another place. \footnote{\label{ref7} FIAN Deutschland:  Klimawandel und Menschenrechte. Die Folgen des Klimawandels für das Recht auf Nahrung und das Recht auf Wasser (2018, ) \url{https://www.fian.de/fileadmin/user\_upload/bilder\_allgemein/Themen/Klima/FIAN\_Klimabroschuere\_2018\_Web.pdf}}

\hypertarget{agrofuels}{%
\paragraph{Agrofuels}\label{agrofuels}}

For more than 10 years now, especially in the global south, more and
more areas have been cultivating maize, soy and sugar cane with the aim
of producing biofuel to satisfy European demand. Also oil palm
plantations are planted for this purpose. Partly such investments are
also financed with development funds from Europe. It is often European
private investors but also credit institutions, such as pension funds,
which invest money in such plantations. As a result of such land
purchases, according to human rights organisations such as FIAN, local
people who previously lived on the land are deprived of its use (often
possible because they do not have secure land titles).

\hypertarget{two-case-studies-from-sierra-leone}{%
\paragraph{Two case studies from Sierra
Leone}\label{two-case-studies-from-sierra-leone}}

In this country, huge areas of land were leased for several decades to a
project of the Swiss company Addax BioEnergy. Over half of the project
was financed by development banks from Europe. The sugar cane planted
there on a large scale will be converted into bioethanol. The local
population, whose right to food and water is being violated, suffers
more. The long leases negotiated between the government of Sierra Leone
and the company - initially 50 years with a potential extension of 20
years - deprive several generations of access to land and water.
\footnote{\label{ref8}FIAN Österreich: Makeni – Sierra Leone: Land Grabbing für europäische Tanks (o.A., ) \url{https://fian.at/de/faelle/sierra-leone-addax/}}

Also in Sierra Leone, the multinational company SOCFIN has acquired more
than 18,000 hectares of land for industrial palm oil plantations in the
south of Sierra Leone (Malen Chiefdom, Pujehun district) since 2011.
Since then, a land conflict has been raging between SOCFIN, the local
authorities and the affected communities. Since the beginning of
SOCFIN's activities, the communities that resisted the land deal have
been systematically criminalised.. \footnote{\label{ref9}FIAN Österreich: Sierra Leone: Landnahme durch Palmöl-Firma SOCFIN beenden (2019, ) \url{https://fian.at/de/artikel/sierra-leone-landnahme-durch-palmol-firma-socfin-beenden/}}

\hypertarget{whats-the-measure}{%
\subsubsection{What's the measure?}\label{whats-the-measure}}

It is essential that all actors involved in a project, be it the
governments of the countries in which such projects are implemented or
the foreign companies and investors involved or their home countries,
comply with their human rights obligations:

\begin{itemize}
\tightlist
\item
  Therefore, an important measure would be to disclose the supply chains
  of companies and to examine them in terms of human rights
  -\textgreater{} politicians must be persuaded to approve the Supply
  Chain Act as it is being promoted by an association of NGOs. More
  information: https://lieferkettengesetz.de
\item
  The creation of an effective and legally binding international
  instrument that forces corporations and other business enterprises to
  respect human rights and gives those negatively affected by the
  projects the opportunity to take legal action in their home countries
  as well as in the countries where the companies are based. (Check out:
  https://bindingtreaty.org/).
\end{itemize}

\begin{itemize}
\tightlist
\item
  Promotion of decentralized renewable energy production, which benefits
  the local population in various ways (energetically, financially) and
  is beneficial to an ecologically intact environment.
\end{itemize}

\begin{itemize}
\tightlist
\item
  Instead of the goal of equipping the same number of cars and other
  motor vehicles as they exist today with lithium-based electric motors,
  alternative ways must be found: a reduction in the number of vehicles,
  car-sharing models, expansion of public transport, hydroelectric fuel
  cells, etc.
\end{itemize}

\begin{itemize}
\tightlist
\item
  There is also a need for stronger references to human rights in
  international climate agreements. So far, these have hardly existed at
  all, and if they have, as in the preamble to the Paris Agreement, then
  in a non-legally binding form.
\end{itemize}

\begin{itemize}
\tightlist
\item
  Reducing energy consumption worldwide.
\end{itemize}
