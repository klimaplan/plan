\subsection{Speed Limit}
\hypertarget{whats-the-problem}{%
\subsubsection{What´s the~problem?}\label{whats-the-problem}}

Throughout Europe and in all industrialised countries worldwide, there
are speed limits on country roads and motorways. Germany is the only
exception, with unlimited speed limits on around 70\% of all motorway
kilometres (Federal Highway Research Institute 2017). Only a few other
countries, including Afghanistan, Haiti, Somalia and North Korea, do not
have such speed limits. With a speed limit of 120 km/h on motorways and
80 km/h on country roads, up to 5 million tonnes could be saved,
according to estimates by Deutsche Umwelthilfe CO₂. No other individual
measure in the transport sector holds such a large and cost-effective
CO₂ savings potential, even in the short term.

\hypertarget{whats-the-measure}{%
\subsubsection{What´s the~measure?}\label{whats-the-measure}}

The legal implementation of a general speed limit on motorways of - for
example - 120 km/h.

\hypertarget{how-can-the-implementation-look-like}{%
\subsubsection{How can the implementation
look~like?}\label{how-can-the-implementation-look-like}}

The implementation is very simple. With a corresponding resolution and
the passing of a law, a general speed limit on the motorways in Germany
could be introduced directly and almost~without any effort in time
and~expense. The same applies to a possible reduction of the existing
limit on country roads from 100 km/h to e.g.~80 km/h~and in towns and
cities from the current 50 km/h to e.g.~40 km/h.

\hypertarget{how-will-this-work-against-climate-change}{%
\subsubsection{How will this work against
climate~change?}\label{how-will-this-work-against-climate-change}}

The CO₂ emissions of passenger cars depend largely on the speed at which
they are driven. Especially at higher speeds, such as those driven on
motorways, the influence of speed on fuel consumption and thus CO₂
emissions is extensive. The reason for this are simple physical
principles. The air resistance of a vehicle increases with the square of
the driving speed, and accordingly, fuel consumption increases
exponentially with an increase in speed. The following example
illustrates this fact: If the speed of an average passenger car
increases from 100 km/h to 130 km/h, CO₂ emissions increase by about
10\%. A further increase of only 10 km/h from 130 to 140 km/h increases
the emissions by another 10\%! (Figures: Federal Environment Agency
Austria²) The laws of physics apply, of course, regardless of the type
of drive of the car. In principle, the electricity consumption of an
electric car is the same as the fuel consumption of a conventional car.
The electricity consumption of an electric car increases enormously at
higher speeds.

On the basis of the latest calculations published by the Federal
Environment Agency and taking into account the 25 percent increase in
traffic performance in the meantime, German Environmental Aid assumes 5
km/h higher average speeds and an improved compliance rate of savings of
up to 5 million tonnes CO₂ per year. In addition to the savings of a
speed limit on motorways, there are the CO₂ savings that can only be
roughly estimated at present if a speed limit of 80 km/h is introduced
on country roads.¹ In addition to these immediate savings, a general
speed limit in Germany would have the potential in the medium and long
term for vehicles to be designed and built differently in the future
(independently of further political steering measures). Since Germany is
a car country and German car manufacturers export a large proportion of
their vehicles, such a trend reversal initiated by a speed limit could
also have effects in other countries. The average motorisation of new
cars in Germany is rising steadily. Whereas in 1997 the average engine
output was still 100 hp, by 2017 it will already be 152 hp. A speed
limit can counteract this trend, since the incentive to build and buy
highly motorised vehicles decreases if the possibility of driving at
high speeds is removed.¹ Why should someone buy a car with 200 hp if he
or she can never try out the performance of the car? A speed limit could
therefore help to ensure that lightweight construction and economy
finally really become the focus of vehicle development. The efficiency
of the drives could also be optimized for the limited cruising speed.

\hypertarget{what-other-positive-effects-does-the-measure-have}{%
\subsubsection{What other positive effects does the
measure~have?}\label{what-other-positive-effects-does-the-measure-have}}

A general speed limit on motorways would increase road safety. There is
a fixed relationship between the speed driven and the frequency of
accidents, but logically also between the speed and intensity of a
possible accident. These are facts that are repeatedly confirmed in real
investigations and are absolutely logical in terms of the basic
principles of physics. A higher speed reduces the reaction time that
remains for a car driver to initiate measures to avert an accident,
which can be braking or taking evasive action, for example. The braking
distance of a vehicle also increases enormously at higher speeds. At a
speed of 120 km/h, it is 108 metres for emergency braking. At 160 km/h
the distance increases to 176 m, and at 200 km/h even to 260 metres.¹ In
addition, the intensity and thus the consequences of a possible accident
increase considerably at higher speeds. The reason for this is the fact
that the kinetic energy of a vehicle is always proportional to the
square of the speed at which it is moving. This means that the energy
released during a collision, for example at the end of a traffic jam,
almost doubles when the speed is increased by 25\% (e.g. from 120 to 160
km/h), because it does not increase linearly but quadratically. The
number of serious accidents and road deaths could be reduced, not only
traffic experts say so, but real investigations confirm it. A study by
the Ministry of Infrastructure and State Planning of the State of
Brandenburg, for example, showed that the introduction of a speed limit
on a good 60 km section of the A 24 motorway could almost halve the
number of accidents.¹ On motorways with a speed limit, 75 percent fewer
fatal accidents occur per billion kilometres driven than on motorway
sections without a speed limit. A speed limit could also reduce the
number of seriously injured in accidents by 20\%.¹ The International
Transport Forum (ITF) also calculates that reducing the average speed on
country roads and motorways by just 5 km/h could reduce the frequency of
fatal accidents by 18 to 28 per cent.

A speed limit on motorways would lead to better traffic flow and fewer
traffic jams. On a motorway without a speed limit, the average speed
difference of vehicles is significantly higher than on a motorway with a
speed limit. A speed limit would reduce the speed difference and lead to
more constant traffic with fewer traffic jams. An improved traffic flow
not only increases the capacity of streets⁴, but also reduces fuel
consumption and CO₂ emissions in traffic.

Not only the CO₂ emissions, but also the emissions of various air
pollutants would be reduced by a speed limit. This is related to the
effect of better traffic flow.

A speed limit would also lead to a slight reduction in road noise.⁵

\hypertarget{how-quickly-can-the-measure-be-implemented}{%
\subsubsection{How quickly can the measure
be~implemented?}\label{how-quickly-can-the-measure-be-implemented}}

The measure can be implemented immediately.~

\hypertarget{how-long-does-it-take-for-the-measure-to-show-positive-effects}{%
\subsubsection{How long does it take for the measure to show
positive~effects?}\label{how-long-does-it-take-for-the-measure-to-show-positive-effects}}

The effect on CO₂ emissions and road safety would be immediate. The
steering effect in the development of new vehicles away from ever more
powerful, heavier and faster cars and towards more efficiency and
lightweight construction could also unfold very soon.
