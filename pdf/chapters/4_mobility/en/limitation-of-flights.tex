\subsection{Limitation of Flights}
\hypertarget{whats-the-problem}{%
\subsubsection{What's the problem?}\label{whats-the-problem}}

Air traffic is the most climate-damaging form of mobility. In order to
achieve a reduction in air traffic, price mechanisms are often
considered first. However, if we want to achieve real climate justice,
we must also ensure that our measures are fair. Price mechanisms have
the disadvantage that poor people are disproportionately affected by
price increases, while rich people could still afford to fly. Price
mechanisms are also subject to market conditions that are constantly
changing, making it difficult to estimate the absolute reduction in
flights caused by price mechanisms.

\hypertarget{what-is-the-measure}{%
\subsubsection{What is the measure?}\label{what-is-the-measure}}

The concrete limitation of flights is theoretically the simplest and
most effective measure to reduce air traffic and to ensure the
contribution of the aviation industry to climate goals. In contrast to
price mechanisms, limitations do not decide between rich and poor - they
apply equally to all. Thus, limitations of flights are to be preferred
from a justice perspective. The aim is to limit the number of flights on
certain routes from certain airports/at certain times, as well as to ban
certain types of flights that do not appear socially useful or
necessary. The concrete measure consists of different parts:

\begin{itemize}
\tightlist
\item
  Abolition of domestic and short-haul flights
\item
  Ban on night flights
\item
  Ban on private jets
\item
  Abolition of business and first class tickets
\end{itemize}

\hypertarget{how-can-this-be-implemented}{%
\subsubsection{How can this be
implemented?}\label{how-can-this-be-implemented}}

\hypertarget{ban-on-domestic-and-short-haul-flights}{%
\paragraph{1. Ban on domestic and short-haul
flights:}\label{ban-on-domestic-and-short-haul-flights}}

In times of climate crisis, domestic and short-haul flights can hardly
be justified. An immediate exit plan and a shift to rail is therefore
needed.

\begin{itemize}
\tightlist
\item
  Immediate ban on flights within a distance of 4-5 hours by rail
\item
  Expansion of the railway infrastructure and agreement with other
  European countries to abolish flights within a distance of 12 hours by
  rail by 2023.
\item
  Transfer of all intra-European flights to rail by 2025
\item
  Exceptions for particularly poorly connected regions, as well as for
  people with physical disabilities.
\end{itemize}

\hypertarget{ban-on-night-flights}{%
\paragraph{2. Ban on night flights:}\label{ban-on-night-flights}}

The possibility of night flights not only increases the capacity of
airports, it also has a significant impact on the health of people
living near airports who are affected by noise. There should therefore
be an immediate ban on night flights at all airports between 10 pm and 7
am.

\hypertarget{ban-on-private-jets}{%
\paragraph{3. Ban on private jets:}\label{ban-on-private-jets}}

Flights in private jets generate many times more greenhouse gas
emissions and are only accessible to a small elite. They should be
banned with immediate effect.

\hypertarget{abolition-of-first-class-and-business-class-tickets}{%
\paragraph{4. Abolition of first class and business class
tickets:}\label{abolition-of-first-class-and-business-class-tickets}}

Due to the increased space requirement, first class and business class
flights have a 3-4 times higher climate effect than "normal" seats. The
1st class and business class areas can be converted to "normal" seats
and thus allow a significantly better utilization of the flights.

\hypertarget{how-does-this-counteract-climate-change-or-how-does-it-create-economic-conditions-that-support-effective-climate-protection-measures}{%
\subsubsection{How does this counteract climate change (or how does it
create economic conditions that support effective climate protection
measures)?}\label{how-does-this-counteract-climate-change-or-how-does-it-create-economic-conditions-that-support-effective-climate-protection-measures}}

Limiting flights has a very direct effect on climate change. The
airplane is by far the most climate-damaging means of transport - if it
is replaced by train or bus, the climate impact will be reduced
immediately.

\hypertarget{what-other-effects-does-the-measure-have}{%
\subsubsection{What other effects does the measure
have?}\label{what-other-effects-does-the-measure-have}}

\begin{itemize}
\tightlist
\item
  Less noise pollution for residents* and the environment
\item
  Fairer distribution of flights
\end{itemize}

\hypertarget{how-quickly-can-the-measure-be-implemented}{%
\subsubsection{How quickly can the measure be
implemented?}\label{how-quickly-can-the-measure-be-implemented}}

Implementation can be started immediately. A ban on night flights, a ban
on private jets and the abolition of 1 class ticketed aircraft can be
implemented immediately. The abolition of domestic and short-haul
flights can also be started immediately. Here, however, a gradual
abolition and shift to rail should be planned in order to have enough
time for investments in rail infrastructure.

\hypertarget{how-long-does-it-take-for-the-measure-to-take-effect}{%
\subsubsection{How long does it take for the measure to take
effect?}\label{how-long-does-it-take-for-the-measure-to-take-effect}}

The flight restrictions will have an immediate effect. If short
distances are shifted to rail, the effect will also be reflected in a
change in mobility behaviour, which will probably be accompanied by a
change in economic processes.

\hypertarget{references-to-other-measures}{%
\subsubsection{References to other
measures}\label{references-to-other-measures}}

The limitation of flights should be accompanied by a halt to the
expansion of airport infrastructure and the abolition of the tax
exemption for the aviation industry, with the simultaneous introduction
of a frequent flyer levy. A simultaneous change in institutional travel
policies is also important.

\hypertarget{problems-of-social-global-or-generational-justice}{%
\subsubsection{Problems of social, global or generational
justice}\label{problems-of-social-global-or-generational-justice}}

When abolishing short and medium-haul flights, it must be borne in mind
that some regions are better and others worse connected to rail
infrastructure. Especially when this measure is applied to the global
context, it is important to realise that a shift to rail is much easier
within Europe than on other continents. However, this is a particular
argument in favour of promoting this shift in Europe.
