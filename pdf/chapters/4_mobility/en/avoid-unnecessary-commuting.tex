\subsection{Avoid unnecessary commuting}


\hypertarget{whats-the-problem}{%
\subsubsection{What´s the~problem?}\label{whats-the-problem}}

According to the ADAC commuter report, 18 million people in Germany
commute at least 10 km to work, averaging 17 km (as of 2017). The
majority of these people (over 60\%) use the car. The associated daily
traffic volume on the roads is enormous. A change to public transport is
not possible in all cases, as especially in rural areas there is a lack
of appropriate offers and even well developed connections have already
reached their current capacity limits. In addition to the expansion of
these offers, the necessity of commuting should be questioned in many
occupational groups.

\hypertarget{whats-the-measure}{%
\subsubsection{What´s the~measure?}\label{whats-the-measure}}

Depending on the profession and task, commuting is unnecessary. Thanks
to digitalisation, many tasks can be performed in the home office. In
addition, in the age of Skype and other digital possibilities,
consulting/sales activities that require customer contact can be done
without physical contact. For these activities, home office must be
enabled and unnecessary mobility must be avoided as far as possible.
Alternatively, these activities could also be carried out in municipal
workspaces provided by non-profit organisations. Furthermore, some
professional activities (which also lead to commuting) are completely
unnecessary, if not harmful to society.

It is useful to divide the measure into four categories of professions:

\textbf{1. occupations in which a large part of the tasks can be carried
out in the home office or in municipal workspaces, but in some cases
physical presence is also required:~}

-Librarians

-Manager

-Production planner

-Secretaries

In these professions, the tasks for which physical presence and thus
commuting is required should be blocked sensibly, e.g. to one day a week
(or two). Only on this day would the relevant persons have to commute to
their place of work - the rest is done at home.

\textbf{2. professions whose tasks can (as far as possible) be carried
out entirely in the home office/municipal workspace:}

-Accountant

-Expert (partially)

-Computer scientist

-Mathematician

-Patent attorneys

-Editors

-Web designer

In these professions, the corresponding offices can be closed (and would
then be available as living space, for example...) and the tasks are
carried out from home or from a communal workspace close to home.
Occasionally necessary team meetings should be kept to a minimum (e.g.
twice a year or once a month) - everything else can be exchanged
digitally.

\textbf{3. professions whose tasks could just as well be performed by
computers/robots/artificial intelligence:}

-Office staff (partially)

-Notaries

-Administrator

For these professions, the corresponding jobs and thus unnecessary
commuting could be abolished. For reasons of justice, the prior
introduction of an unconditional basic income (see Unconditional Basic
Income) is necessary for this.

\textbf{4. professions where simplification, standardisation,
de-bureaucratisation and/or merging/centralisation would eliminate a
large proportion of the tasks:~}

-Employees in public offices (e.g. residents' registration office,
employment office)

-Insurance agents and employees

-Marketing consultant

-Tax consultant

In these areas, task avoidance should be the objective. This would
eliminate a large number of jobs and thus also a lot of commuter
traffic. For reasons of fairness, the prior introduction of an
unconditional basic income (see Unconditional Basic Income) is of course
necessary.

\hypertarget{what-can-the-implementation-look-like}{%
\subsubsection{What can the implementation
look~like?}\label{what-can-the-implementation-look-like}}

Step 1: Check (by whom????) for which professions or areas of
responsibility the categories described above apply. Further
implementation depends on category:

 \textbf{ Work category 1 (see above):}

-provision of the corresponding home office software

-check, whether home office is possible and reasonable or whether
alternatively communal working spaces are available in the respective
residential area or the creation of such

-training of the employees concerned

-personal and temporal redistribution of tasks within the respective
companies and businesses, so that as little commuting as possible is
necessary

-External control???

 \textbf{ Work category 2 (see above):}

-provision of the corresponding home office software

-check, whether home office is possible and reasonable or whether
alternatively communal working spaces are available in the respective
residential area or the creation of such

-training of the employees

-closure of the offices - conversion for other use of the rooms possible
(e.g. living space!)

 \textbf{ Work category 3 (see above):}

-development of techniques and software that can replace the work of
human beings

-model testing of these techniques and gradual introduction

 \textbf{ Work category 4 (see above):}

This requires major systemic changes, which would have to be designed
differently depending on the area (possibly formulate as extra
measures). The concept of "Central Humanity Insurance" should serve as
an example here:

This insurance could replace the current system of unnecessary diversity
of health, long-term care, pension, unemployment and disability
insurance. All citizens of Germany would pay into this insurance
according to their possibilities or the state would take care of it for
them (a suitable system for this would still have to be developed). This
humanity insurance would then cover all cases of illness, old age,
accidents, etc., whereas unemployment insurance would be dropped anyway
when the Unconditional Basic Income is introduced. There would be a
central office for this insurance somewhere in Germany. The few
activities that cannot be done by computers can be done by people in the
home office. In the central office itself, there would hardly be any
need for physical presence.~The massive centralization and
simplification would eliminate a large number of workstations,
especially in various statutory and private health insurance companies
and in the employment office.

\hypertarget{how-will-this-counteract-climate-change}{%
\subsubsection{How will this counteract
climate~change?}\label{how-will-this-counteract-climate-change}}

Assuming that one third of commuter traffic can be avoided by the
measures described (assumption, would have to be checked!) and that 18
million people in Germany commute an average of 17 km (i.e. 34 km back
and forth) on 230 working days per year, two thirds of which by car
(most of them individually in the car), 34 billion car kilometres could
be avoided annually by these measures. With a fuel consumption of 8
litres per 100 km, this would save around 6.3 million tonnes of carbon
dioxide annually in Germany. In addition, CO$_2$ is saved for commuters who
travel frequently by plane for work reasons.

\hypertarget{what-other-effects-does-the-measure-have}{%
\subsubsection{What other effects does the
measure~have?}\label{what-other-effects-does-the-measure-have}}

Less commuting also means for those affected:

-less stress in everyday life, resulting in better health (and higher
performance, if the value "performance" is to be maintained in a new,
healthier society...)

-more leisure time

-fewer accidents

For city dwellers, lower traffic volumes mean additional benefits:

-cleaner air -\/-\textgreater{} fewer respiratory diseases

-less noise -\/-\textgreater{} better health

-fewer accidents, also for uninvolved pedestrians, cyclists and playing
children

Housing could be created by closing down offices and rebuilding.

\hypertarget{how-quickly-can-the-measure-be-implemented}{%
\subsubsection{How quickly can the measure
be~implemented?}\label{how-quickly-can-the-measure-be-implemented}}

The measures for occupational areas 1 and 2 (see above) could be
implemented almost immediately if the appropriate legislation is in
place and the response from the business community is positive.
Difficulties in this regard could be the provision of software for home
office work and the training of employees in this.

Since the measures in occupational areas 3 and 4 (see above) represent
radical cuts in the current system, it is to be expected that
implementation will take several years. In particular, the Unconditional
Basic Income must have been introduced beforehand, so that no one will
mourn the loss of jobs that would be lost through the centralisation and
simplification described above.

\hypertarget{how-long-does-it-take-for-the-measure-to-take-an-effect}{%
\subsubsection{How long does it take for the measure to take
an~effect?}\label{how-long-does-it-take-for-the-measure-to-take-an-effect}}

Once the measures are implemented, there will be a corresponding
reduction in annual carbon dioxide emissions.

\hypertarget{references-to-other-measures}{%
\subsubsection{References to
other~measures}\label{references-to-other-measures}}

-
\href{https://wiki.gerechte1komma5.de/tiki-index.php?page=Basic\%20and\%20maximum\%20income\&no_bl=y}{unconditional
basic income}

-
\href{https://wiki.gerechte1komma5.de/tiki-index.php?page=Reduction\%20of\%20working\%20hours\&no_bl=y}{reduction
of working hours}

\hypertarget{problems-of-social-global-and-generational-justice}{%
\subsubsection{Problems of social, global and generational
justice}\label{problems-of-social-global-and-generational-justice}}

Job cuts could be perceived as unfair, especially in the transition
phase, if social security through a basic income has not yet been
introduced and our current value system is not reconsidered.
(-\/-\textgreater{} Should the goal really be, as before, that everyone
has work? Or should it not rather be that as little work as possible be
required?)

Older people might have problems in the conversion to home office,
because this requires relearning.

Home-office work could lead to increased social isolation and physical
inactivity of people. On the other hand, increased leisure time could
counteract this. Social isolation would be less important if municipal
work spaces were created close to home.

With home office work, it is difficult to separate professional and
private life, which is why there is an increased risk of burnout. This
should be counteracted by reducing working hours.

The creative exchange between employees and between boss and employees
is less direct in home office work. In jobs where this is necessary,
occasional meetings should be ensured to facilitate this exchange.

Creating the spatial conditions for home office could be difficult for
some people in view of the housing situation - in this case the creation
of communal working spaces would be more sensible instead. On the other
hand, by closing down offices that would then no longer be needed,
spatial resources would be available that could be converted into living
space, which could ease the situation.

The abolition of private and statutory health insurance could be
perceived within individual population groups as a restriction of the
freedom of choice.
