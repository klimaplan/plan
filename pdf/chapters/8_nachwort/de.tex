Die Entstehung des \emph{Klimaplans von unten} war ursprünglich in fünf
zeitlich aufeinander folgende Phasen gegliedert:

\begin{enumerate}
\tightlist
\item
  Idee bekannt machen und Mitschreibende suchen
\item
  Maßnahmen aufschreiben
\item
  Feedback einholen
\item
  Feinschliff
\item
  Veröffentlichung
\end{enumerate}

Im Laufe der Zeit zeigte sich, dass es nicht praktikabel ist, die
Schritte nacheinander abzuarbeiten. Das lag daran, dass spätere Schritte
(wie Feedback einholen) frühere Phasen (wie Maßnahmen aufschreiben)
beeinflussen. Beispielsweise ist auch die Hürde für die Beteiligung
niedriger, wenn bereits Inhalte vorhanden sind, die den Menschen Ideen
geben, in welche Richtung sich das Projekt entwickeln könnte.

Des Weiteren stellten wir bald fest, dass nach den geplanten sechs
Monaten Schreibprozess kein fertiges Dokument zur Verfügung stehen
würde. Auch musste der Anspruch an den Klimaplan angepasst werden. Denn
ob überhaupt ein Dokument entstehen könnte, welches dann nur noch in die
Tat umgesetzt werden müsste und das gleichzeitig mit erdrückender
Beweislast zeigen würde, dass im gegenwärtigen System (die
Veröffentlichung sollte sich auch an die Bundesregierung richten) keine
nennenswerten Schritte in Richtung Klimagerechtigkeit möglich sind, war
fraglich.

Stattdessen zeigte sich, dass die Verhandlungen darüber, was
Klimagerechtigkeit ist, überhaupt erst geführt werden müssen --
jedenfalls in einem Kreis, der nicht nur die radikaleren
Klimagerechtigkeitsgruppen umfasst. Genau dafür möchte der
\emph{Klimaplan von unten} eine Plattform sein und deswegen ein
lebendiges, veränderbares Dokument bleiben.

Im folgenden möchten wir die einzelnen Phasen der Entstehung kritisch
durchleuchten und unsere Erfahrungen teilen:

\hypertarget{mitschreibende-suchen-und-2.---mauxdfnahmen-aufschreiben}{%
\paragraph{1. - Mitschreibende suchen und 2. - Maßnahmen
aufschreiben}\label{mitschreibende-suchen-und-2.---mauxdfnahmen-aufschreiben}}

Auf der Suche nach Mitschreibenden erhielten wir viel positive Resonanz
auf unsere Idee. Die Ansicht, dass ein solcher Plan ein wichtiger
Schritt in der Klimagerechtigkeitsbewegung sei, wurde oft geteilt.
Gleichzeitig stießen wir nur selten auf die Bereitschaft, aktiv am Plan
mit zuschreiben. Das mag teilweise an vollen Terminplänen liegen, kann
aber nicht bloß auf diesen Umstand zurückgeführt werden.

Eine Methode, um Mitschreibende zu animieren, waren sogenannte
Write-Ins. Dieses Format basierte auf der Idee, mit interessierten
Menschen in einem physischen Treffen an dem Klimaplan zu arbeiten und
bereits bestehende Inhalte zu diskutieren. Die Ergebnisse dieser
Write-Ins legen nahe, dass das Format für einige Teilnehmende zu
akademisch und daher zu hochschwellig war und auch abschreckend auf
andere Menschen wirkte. Dennoch wurden die Write-Ins von Teilnehmenden
auch oft als Erfahrung der Selbstermächtigung beschrieben.

\hypertarget{feedback}{%
\paragraph{3. - Feedback}\label{feedback}}

Der \emph{Klimaplan von unten} hat den Anspruch global und sozial
gerecht zu sein und möglichst viele, unterschiedliche Perspektiven mit
einzubeziehen. Dazu gehören vor allem auch Sichtweisen von Menschen, die
besonders von den Auswirkungen der Erderwärmung betroffen sind, sowie
die Sichtweisen von marginalisierten und auf verschiedenste Weisen
diskriminierten Gruppen. Jetzt, nach fast einem Jahr müssen wir
feststellen, dass es uns nicht ausreichend gelungen ist -- sowohl im
Schreibprozess selbst, als auch durch das Einholen von Feedback zu
fertigen Maßnahmen -- eine diverse Gruppe von Menschen in den
Schreibprozess des \emph{Klimaplan von unten} mit einzubeziehen. Wir
haben bislang noch nicht genug konkretes Feedback bekommen, um wirklich
von globaler und intersektionaler Gerechtigkeit sprechen zu können. Um
die von uns gewünschte Auseinandersetzung zu erreichen, versuchen wir
weiterhin Kontakte aufzubauen und aufrecht zu erhalten.

Den Schluss, den wir daraus ziehen ist, dass wir unser Vorgehen kritisch
hinterfragen müssen. Wie so oft in der deutschen
Klimagerechtigkeitsbewegung, die vor allem von einer weißen
Mittelschicht geprägt ist, war unser Vorgehen nicht inklusiv genug, um
für alle Menschen ansprechend zu sein. Mit der ersten Auflage wollen wir
dieses sich wiederholende Phänomen aufzeigen und gemeinsam reflektieren,
um daran zu wachsen. Wir wünschen uns solidarisch-kritisches Feedback
und freuen uns über Hinweise, wie wir inklusiver in unserem Schreiben,
Denken und Handeln sein können. Darüber hinaus wollen wir eine herzliche
Einladung an alle Menschen aussprechen, die ihre Perspektive in dieser
ersten Auflage des \emph{Klimaplan von unten} noch nicht vertreten
sehen, sich mit den jeweiligen Themen einzubringen. Wir als Kampagne
wollen lernen, wie wir bisher oft marginalisierte Kämpfe sichtbar machen
und die beteiligten Personen unterstützen können.

\hypertarget{feinschliff-einfache-zuguxe4ngliche-sprache}{%
\paragraph{4. - Feinschliff -- einfache, zugängliche
Sprache}\label{feinschliff-einfache-zuguxe4ngliche-sprache}}

Ziel des Feinschliffs sollte, neben optischer Aufbereitung und
Fehlerkorrektur, vor allem eine einfache Sprache sein, die so wenig
Verständnishürden wie möglich aufbaut. Mit zunehmender Zahl von
Textbeiträgen wurde uns klar, dass viele Maßnahmen in wissenschaftlicher
Sprache geschrieben werden müssen, um den Inhalt angemessen zu
vermitteln. Außerdem setzen viele Maßnahmen ein umfangreiches Wissen
über grundlegende Konzepte voraus. Gleichzeitig stellt das Format ‚Text`
überhaupt schon eine große Hürde dar. Diesen Problemen wollen wir auf
verschiedene Weisen begegnen:

\begin{itemize}
\tightlist
\item
  Durch die Übersetzung des \emph{Klimaplan von unten} in einfache
  Sprache. Dadurch können die originalen Maßnahmentexte erhalten bleiben
  und gleichzeitig eine inhaltlich reduzierte, aber leicht verständliche
  Version angeboten werden. Da uns für dieses größere Projekt derzeit
  die Kapazitäten fehlen, muss es auf die zweite Auflage verschoben
  werden.
\item
  Durch Anlegen eines Glossars, das die wichtigsten Schlagworte sehr
  knapp beschreibt. Das erspart das Nachschlagen anderswo und reduziert
  den zusätzlichen Leseaufwand auf ein Minimum.
\item
  Durch das Vermitteln der Inhalte des Klimaplans in anderen Formaten,
  insbesondere mit Hilfe von Infografiken und Erklärvideos. Auch damit
  wurde gerade erst begonnen und wir hoffen, nach der Veröffentlichung
  immer mehr Material ergänzen zu können.
\end{itemize}

Eine weitere Aufgabe des Feinschliffs sollte das Einarbeiten von
Feedback und das Ausräumen inhaltlicher Dopplungen und Widersprüche
sein. Die Schwierigkeiten in diesem Prozess lagen darin, dass viele
Beiträge eine grundlegende Kritik sind und manche inhaltlichen Fragen
sich nur mit fundiertem Fachwissen (über das die Feinschleifenden nicht
verfügen) klären lassen. Und schließlich kann im Fall von Widersprüchen
nicht eine kleine Gruppe entscheiden, was „richtig`` ist. Das muss über
den größeren Diskurs passieren. Daher werden widersprüchliche Ideen in
dieser Auflage abgebildet, um dann hoffentlich rege diskutiert zu
werden.

\hypertarget{veruxf6ffentlichung}{%
\paragraph{5. - Veröffentlichung}\label{veruxf6ffentlichung}}

Die Veröffentlichung erfolgt als Website und als Printmedium in sehr
kleiner Auflage beziehungsweise als PDF. Dabei sind über die Website
sämtliche bisher erstellten Inhalte einsehbar und genauso die
Kommentare, die bisher gemacht (aber nicht eingearbeitet) wurden. Die
Seite soll zusätzlich Schrittweise um Illustrationen und Erklärvideos
ergänzt werden.

In der Print-Auflage sind nur die redaktionell bearbeiteten
(„feingeschliffenen``) Texte enthalten. Da wir hoffen, dass diese
Auflage relativ schnell überholt sein wird, soll sie nur in sehr kleiner
Zahl gedruckt werden und kaum öffentlich Verbreitung finden.
