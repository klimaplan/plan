#!/bin/bash
cd $(dirname $0)

USER_ID=$(id -u)
GROUP_ID=$(id -g)

DIR=$(realpath ..)

# rm ../output/*
docker run \
    -v $DIR:/doc/ \
    thomasweise/docker-texlive-full \
    sh -c "cd /doc && pdflatex -mktex=tfm -output-directory=/doc/output  /doc/main/de; chown -R $USER_ID:$GROUP_ID /doc/output"

